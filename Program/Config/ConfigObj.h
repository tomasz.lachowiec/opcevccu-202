#ifndef CONFIGOBJ_H
#define CONFIGOBJ_H

#include <QObject>
#include <QTime>
#include <QStringList>

//class ConfigObj : public QObject
class ConfigObj
{
    //Q_OBJECT
public:
    enum{
        CFG_VER_MAJOR = 1,
        CFG_VER_MAINOR = 1
    };
    enum{
        OK,
        UNABLE_2_WRITE, // brak uprawnien do zapisu
        NO_FILE,        // brak pliku w podanej sciezce
        FILE_CORRUPTED, // uszkodzony lub niewlasciwy plik
        FILE_INVALID_VER // nieprawidlowa wersja pliku (roznica w CFG_VER_MAJOR)
    };

    enum{
        AUTO_MEASURE_CALIB = 0,
        CONST_MEASURE_CALIB = 1
    };

    explicit ConfigObj();

    int loadFromFile(QString filePath);
    int save2File(QString filePath);

    //--------------------------------------------------------------------
    // Klasy podgrup konfiguracji
    //--------------------------------------------------------------------
    class GeneralSettings{
    public:
        GeneralSettings() : deviceID_Cfg("AP01"), deviceDesc_Cfg("Cup calibrator"), lang_Cfg("en"), versionFirmware_Cfg("0.0.0.1"),
            filePathFirmware_Cfg(""), enableModule_1_Cfg(false), enableModule_2_Cfg(false), enableModule_3_Cfg(false),
            calibrateModeModule_1_Cfg(CONST_MEASURE_CALIB), calibrateModeModule_2_Cfg(CONST_MEASURE_CALIB), calibrateModeModule_3_Cfg(CONST_MEASURE_CALIB),
            calibrateResistancePoint_1_Module_1_Sensor_1_Cfg(0.0), calibrateResistancePoint_2_Module_1_Sensor_1_Cfg(0.0),
            calibrateResistancePoint_1_Module_2_Sensor_1_Cfg(0.0), calibrateResistancePoint_2_Module_2_Sensor_1_Cfg(0.0),
            calibrateResistancePoint_1_Module_3_Sensor_1_Cfg(0.0), calibrateResistancePoint_2_Module_3_Sensor_1_Cfg(0.0),
            calibrateResistancePoint_1_Module_1_Sensor_2_Cfg(0.0), calibrateResistancePoint_2_Module_1_Sensor_2_Cfg(0.0),
            calibrateResistancePoint_1_Module_2_Sensor_2_Cfg(0.0), calibrateResistancePoint_2_Module_2_Sensor_2_Cfg(0.0),
            calibrateResistancePoint_1_Module_3_Sensor_2_Cfg(0.0), calibrateResistancePoint_2_Module_3_Sensor_2_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_1_Sensor_1_Cfg(0.0), calibrateTemperaturePoint_2_Module_1_Sensor_1_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_2_Sensor_1_Cfg(0.0), calibrateTemperaturePoint_2_Module_2_Sensor_1_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_3_Sensor_1_Cfg(0.0), calibrateTemperaturePoint_2_Module_3_Sensor_1_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg(0.0), calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg(0.0), calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg(0.0),
            calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg(0.0), calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg(0.0),
            enableTestModule_1_Cfg(false), enableTestModule_2_Cfg(false), enableTestModule_3_Cfg(false),
            testResistancePointVal_1_Module_1_Sensor_1_Cfg(0.0), testResistancePointVal_2_Module_1_Sensor_1_Cfg(0.0),
            testResistancePointVal_1_Module_2_Sensor_1_Cfg(0.0), testResistancePointVal_2_Module_2_Sensor_1_Cfg(0.0),
            testResistancePointVal_1_Module_3_Sensor_1_Cfg(0.0), testResistancePointVal_2_Module_3_Sensor_1_Cfg(0.0),
            testResistancePointVal_1_Module_1_Sensor_2_Cfg(0.0), testResistancePointVal_2_Module_1_Sensor_2_Cfg(0.0),
            testResistancePointVal_1_Module_2_Sensor_2_Cfg(0.0), testResistancePointVal_2_Module_2_Sensor_2_Cfg(0.0),
            testResistancePointVal_1_Module_3_Sensor_2_Cfg(0.0), testResistancePointVal_2_Module_3_Sensor_2_Cfg(0.0) {}

        QString deviceID_Cfg;   // nazwa urządzenia
        QString deviceDesc_Cfg; // opis urządzenia
        QString lang_Cfg;       // "en", "pl", ...

        QString versionFirmware_Cfg;       // wersja firmware
        QString filePathFirmware_Cfg;          // sciezka do skryptu programujacego

        bool enableModule_1_Cfg;            //wlaczenie modułu do kalibracji
        bool enableModule_2_Cfg;            //wlaczenie modułu do kalibracji
        bool enableModule_3_Cfg;            //wlaczenie modułu do kalibracji

        char calibrateModeModule_1_Cfg;         //tryb kalibracji modułu
        char calibrateModeModule_2_Cfg;         //tryb kalibracji modułu
        char calibrateModeModule_3_Cfg;         //tryb kalibracji modułu

        float calibrateResistancePoint_1_Module_1_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_1_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 2
        float calibrateResistancePoint_1_Module_1_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_1_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 2

        float calibrateResistancePoint_1_Module_2_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_2_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 2
        float calibrateResistancePoint_1_Module_2_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_2_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 2

        float calibrateResistancePoint_1_Module_3_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_3_Sensor_1_Cfg;   //wartosc rezystancji kalibracji dla punkt 2
        float calibrateResistancePoint_1_Module_3_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 1
        float calibrateResistancePoint_2_Module_3_Sensor_2_Cfg;   //wartosc rezystancji kalibracji dla punkt 2


        float calibrateTemperaturePoint_1_Module_1_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_1_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 2
        float calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 2

        float calibrateTemperaturePoint_1_Module_2_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_2_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 2
        float calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 2

        float calibrateTemperaturePoint_1_Module_3_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_3_Sensor_1_Cfg; //wartosc temperatury kalibrowanej dla punkt 2
        float calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 1
        float calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg; //wartosc temperatury kalibrowanej dla punkt 2

        bool enableTestModule_1_Cfg;            //wlaczenie testowanie modułu
        bool enableTestModule_2_Cfg;            //wlaczenie testowanie modułu
        bool enableTestModule_3_Cfg;            //wlaczenie testowanie modułu


        float testResistancePointVal_1_Module_1_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 1
        float testResistancePointVal_2_Module_1_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 1
        float testResistancePointVal_1_Module_1_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 1
        float testResistancePointVal_2_Module_1_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 1

        float testResistancePointVal_1_Module_2_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 2
        float testResistancePointVal_2_Module_2_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 2
        float testResistancePointVal_1_Module_2_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 2
        float testResistancePointVal_2_Module_2_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 2

        float testResistancePointVal_1_Module_3_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 3
        float testResistancePointVal_2_Module_3_Sensor_1_Cfg;          //resystancja dla punktu testowego moduł no 3
        float testResistancePointVal_1_Module_3_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 3
        float testResistancePointVal_2_Module_3_Sensor_2_Cfg;          //resystancja dla punktu testowego moduł no 3
    };


    //====================================================================
    GeneralSettings generalSettings;

private:

};

#endif // CONFIGOBJ_H

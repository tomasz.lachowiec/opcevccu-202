#ifndef MODBUSCLIENT_H
#define MODBUSCLIENT_H

#include <QObject>
#include <QMutex>
#include <QTimer>
#include <QList>
#include <QMutex>
#include <QSerialPortInfo>

//#include "Modbus/ModbusRTUMaster.h"
#include "Config/ConfigSoftware.h"

#include "inc/def.h"
#include "3rdparty/libmodbus/src/modbus.h"
#include "../GUI/Pages/Modules/calibrationpage.h"

class ModbusClient : public QObject
{
    Q_OBJECT
public:

    enum ModbusConnection {
    Serial,
    Tcp
    };

    explicit ModbusClient(QString portName, ModbusClient::ModbusConnection typeConnection, QObject *parent = nullptr);
    ~ModbusClient();

    bool connectMDB();
    void disconnectMDB();
    QList<uint16_t>  readRegisters(int addrModbus, int regAddr, int regCnt, unsigned int retNumb = 0);
    bool writeRegisters(int addrModbus, int regAddr, uint16_t* regs, int regCnt, unsigned int retNumb = 0);
    //bool writeRegisters(int addrModbus, int regAddr, float* regs, int regCnt, unsigned int retNumb = 0);
    void readWriteModbusRegisters(int addresDevice, int addresRegister, int quantityRegisters);
    void setPortName(QString portName);
    modbus_t *modbusHandler = nullptr;
    bool isConnected;
    SettingsDevice m_settingsDevice;
    QMutex mutexModbusDeviceUse;

   // CalibrationPage *page ;

signals:



public slots:


private:
    ModbusConnection m_typeConnection;
    QString m_portName;
};

#endif // MODBUSCLIENT_H

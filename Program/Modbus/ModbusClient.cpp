#include "ModbusClient.h"

#include <QDebug>
#include <QThread>
//#include <QModbusClient>

#include "inc/global.h"


#define IO_CARD_POLL_DELAY_MS           20 // opozninie pomiedzy kolejnymi zapytaniami
#define IO_CARD_UPDATE_TIME_MS          500
#define IO_CARD_RESPONSE_TIMEOUT_MS     100

#define SLAVE_ID 1
#define MAX_RETRAN_CNT          3

#define SERIAL_PORT_NAME "\\\\.\\COM10"


ModbusClient::ModbusClient(QString portName, ModbusClient::ModbusConnection typeConnection, QObject *parent) : QObject(parent)
{
    //definicja obiektow modbus dla odczytu z modułu AP01 i z płytki sterującej
    m_portName = portName;
    m_typeConnection = typeConnection;
}

ModbusClient::~ModbusClient()
{

//    if(serialModbusClientDevice)
//        delete serialModbusClientDevice;
}


/** Polaczenie z portem  i identyfikacja karty */
bool ModbusClient::connectMDB()
{
    if( modbusHandler ){
        modbus_close( modbusHandler );
        modbus_free( modbusHandler );
    }
    QByteArray com="\\\\.\\" + m_portName.toLatin1();

    modbusHandler =  modbus_new_rtu((const char*)com, m_settingsDevice.baud, 'N', 8, 2);
//    modbusHandler =  modbus_new_rtu(SERIAL_PORT_NAME, 9600, 'N', 8, 1);
    if( modbus_connect( modbusHandler ) == -1 ) {
        modbusHandler = NULL;
        isConnected = false;
        qDebug() << "Could not connect to serial port: " << com << "!!!!!!!!!!!";
    }
    else{
        qDebug() << "Connected to serial port: " << com;

        isConnected = true;

 //       struct timeval response_timeout;


        uint32_t to_sec;
        uint32_t to_usec;

        modbus_get_response_timeout(modbusHandler, &to_sec, &to_usec);

        to_usec = m_settingsDevice.responseTime % 1000 * 1000; // zamiana na usec
        to_sec = m_settingsDevice.responseTime / 1000; // zamiana na sec
        modbus_set_response_timeout(modbusHandler, to_sec, to_usec);

    }

    return isConnected;
}
void ModbusClient::disconnectMDB()
{
    if( modbusHandler ){
        modbus_close( modbusHandler );
        modbus_free( modbusHandler );
    }

    modbusHandler = NULL;
    isConnected = false;
}

QList<uint16_t> ModbusClient::readRegisters(int addrModbus, int regAddr, int regCnt, unsigned int retNumb)
{
    QList<uint16_t> retArray;

    if( !modbusHandler ){
        return retArray;
    }

    int valSize = 16; // z ilu bitow sklada sie jedna wartosc (16, 32)
    int valuesCnt = regCnt;   // ile wartosci ma powstac z odpytanych rejestorw
    QString regType = "int";

    uint16_t * buffer;      // bufor na wartosci rejestrow 16 bitowych
    const int maxRegistersCnt = 200; // przykladowa wartosc
    if(valuesCnt <= maxRegistersCnt){
         buffer = new uint16_t[regCnt * (valSize / 16) ]; // 16 => regCnt * 1, 32 => regCnt * 2
    }
    else
        return retArray;

    modbus_set_slave( modbusHandler,  addrModbus);
    int ret = modbus_read_registers( modbusHandler, regAddr, regCnt, buffer );

    if( ret == regCnt  )
    {
        for( int i = 0; i < valuesCnt; i++ ){
            if(regType == "int" && valSize == 16)
                retArray.append((int16_t) buffer[i]);
            else // uint 16bit
                retArray.append(buffer[i]);
        }
        delete[] buffer;
        return retArray;
    }
    else
    {
        delete[] buffer;


        if( ret < 0 )
        {
            if(errno == EIO){ // timeout
                return retArray;
             }
            else{ // blad CRC - proba retransmisji
                //if( (retNumb < MAX_RETRAN_CNT)  && this->reconnectPort() ){
                if( (retNumb > 0) ){    //MAX_RETRAN_CNT
                    qDebug() << "IO Card: CRC read retransmission attempt number : " << (retNumb + 1) << "regAddr: " << regAddr;
                    QThread::msleep(IO_CARD_POLL_DELAY_MS);
                    return this->readRegisters(addrModbus, regAddr, regCnt, --retNumb);
                }
            }
        }
        else{
            //if( (retNumb < MAX_RETRAN_CNT)  && this->reconnectPort() ){
            if( (retNumb > 0) ){
                qDebug() << "IO Card: Reg numb retransmission attempt number : " << (retNumb + 1);
                QThread::msleep(IO_CARD_POLL_DELAY_MS);
                return this->readRegisters(addrModbus, regAddr, regCnt, --retNumb);
            }
        }
        return retArray;
    }
}

bool ModbusClient::writeRegisters(int addrModbus, int regAddr, uint16_t *regs, int regCnt, unsigned int retNumb)
{
    if( !modbusHandler ){
        return false;
    }
    modbus_set_slave( modbusHandler, addrModbus );
    int ret = modbus_write_registers( modbusHandler, regAddr, regCnt, regs );
    //qDebug() << " ret_mdb : " << ret;
    if( ret == regCnt  )
    {
        return true;
    }
    else
    {
        if( ret < 0 )
        {
            if(errno == EIO){ // timeout - bez retransmisji

                return false;
            }
            else{ // blad CRC - proba retransmisji
                //if( (retNumb < MAX_RETRAN_CNT)  && this->reconnectPort() ){
                if( (retNumb > 0) ){
                    qDebug() << "IO Card: CRC write retransmission attempt number : " << (retNumb + 1) << ", regAddr: " << regAddr;
                    QThread::msleep(IO_CARD_POLL_DELAY_MS);
                    return this->writeRegisters(addrModbus, regAddr, regs, regCnt, --retNumb);
                }

            }
        }
        else{
            //if( (retNumb < MAX_RETRAN_CNT)  && this->reconnectPort() ){
            if( (retNumb > 0) ){
                qDebug() << "IO Card: Reg numb retransmission attempt number : " << (retNumb + 1);
                QThread::msleep(IO_CARD_POLL_DELAY_MS);
                return this->writeRegisters(addrModbus, regAddr, regs, regCnt, --retNumb);
            }
        }

        return false;
    }
}


void ModbusClient::setPortName(QString portName)
{

    m_portName = portName;

}

#ifndef MODBUSREGISTER_H
#define MODBUSREGISTER_H


#include <QMutex>
#include <QVector>


class ModbusRegisters
{
public:
    ModbusRegisters(int quantityBlockReg, int sizeBlockReg);
    ~ModbusRegisters();


    QMutex mutexRegister;
    QVector<QVector<quint16>> m_holdingRegisters;



};

#endif // MODBUSREGISTER_H

#include "DeviceSelectDialog.h"
#include "ui_DeviceSelectDialog.h"
#include <inc/def.h>
#include <QStyle>
#include <QWidget>

DeviceSelectDialog::DeviceSelectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeviceSelectDialog)
{
    ui->setupUi(this);

    selectedDevice = QDialog::Rejected;

    this->setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint );
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->AppDevices_toolBox->setItemEnabled(0, false);
    ui->AppDevices_toolBox->setItemEnabled(1, false);
}

DeviceSelectDialog::~DeviceSelectDialog()
{
    delete ui;
}

int DeviceSelectDialog::myExec()
{
    this->exec();
    return selectedDevice;
}

void DeviceSelectDialog::on_MD04_button_clicked()
{
    selectedDevice = MD04;
    this->close();
}

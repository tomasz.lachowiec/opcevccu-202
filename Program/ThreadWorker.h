#ifndef THREADWORKER_H
#define THREADWORKER_H

#include <QObject>

class ThreadWorker : public QObject
{
    Q_OBJECT
public:
    explicit ThreadWorker(QObject *parent = 0);
    virtual ~ThreadWorker();

    virtual void quit() = 0; // implementacja w klasie dziedziczacej

signals:
    void finished();
    void error(QString err);

public slots:
    virtual void process() = 0; // implementacja w klasie dziedziczacej
};

#endif // THREADWORKER_H

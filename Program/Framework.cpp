#include "Framework.h"


#include <QDebug>
#include <QDir>

#include <QStyle>
#include <QDesktopWidget>
#include <QTimer>
#include <QThread>
#include <QSerialPortInfo>

Framework::Framework(QObject *parent) : QObject(parent)
{
    g_configSoftware = new ConfigSoftware();    //globalna konfiguracja oprogramowania
    g_connectionStatus = 0;
}

void Framework::centerMainWindow(QDesktopWidget *desktop)
{
    int WIDTH = 1000;
    int HEIGHT = 850;

    int screenWidth = desktop->width();
    int screenHeight = desktop->height();

    int x = (screenWidth - WIDTH) / 2;
    int y = (screenHeight - HEIGHT) / 2;

//    m_mainWindow->resize(WIDTH, HEIGHT);
    m_mainWindow->move( x, y );

//    m_mainWindow->showMaximized();

}

void Framework::createMainWindow(QDesktopWidget *desktop)
{
    MainWindow w;
    w.show();

    m_mainWindow = new MainWindow();

    this->centerMainWindow(desktop);
    m_mainWindow->show();

//    QTimer::singleShot(1000, this, SLOT(showFullScreen()));

    connect(this, SIGNAL(consoleMessage(QString, int)), m_mainWindow, SLOT(addConsoleMessage(QString,int)));

    qRegisterMetaType<QList<uint16_t> >("QList<uint16_t>");

    for (QSerialPortInfo port : QSerialPortInfo::availablePorts())
    {
        // print the port name
        qDebug() << port.portName();
    }

    //odczytujemy konfiguracje z pliku
    QString pathFileInterface = QDir::currentPath()+ "/settings.ini";

    qDebug() << pathFileInterface;

    if(g_configSoftware->loadFromFile(pathFileInterface) != ConfigSoftware::OK)
    {
        //blad parametrow konfiguracyjnych
        //ustaiwmay parametry domyslne
        g_configSoftware->generalSettings.addresDevice = 1;
        g_configSoftware->generalSettings.namePortsCom = "\\\\.\\COM1";

        g_configSoftware->generalSettings.settingsPortCom.baud = QSerialPort::Baud115200;
        g_configSoftware->generalSettings.settingsPortCom.dataBits = QSerialPort::Data8;
        g_configSoftware->generalSettings.settingsPortCom.parity = QSerialPort::NoParity;
        g_configSoftware->generalSettings.settingsPortCom.stopBits = QSerialPort::OneStop;
        g_configSoftware->generalSettings.settingsPortCom.numberOfRetries = 1;
        g_configSoftware->generalSettings.settingsPortCom.responseTime = 1000;

    }



     runThreads();

     m_mainWindow->changePage(MainWindow::PAGE_IDX_WELCOME);

}


/** Uruchomienie (pozostalych) wątków */
void Framework::runThreads(void)
{

// this->runThread( (ThreadWorker*) new TestProcess(), true);

//    g_modbusScanTimer = new QTimer(0);
//    g_modbusScanTimer->setInterval(1000);


    //utowrzenie obiektow dla płytki setrownika
    g_modbusClientDevice = new ModbusClient(g_configSoftware->generalSettings.namePortsCom, ModbusClient::Serial); //utworzenie obiktu modbus client
    g_modbusClientDevice->m_settingsDevice = g_configSoftware->generalSettings.settingsPortCom;    //przypisanie aktualnej konfiguracji

    // TODO: Tutaj dodawac nowe watki
}

/** Uruchamia jeden watek roboczy */
void Framework::runThread(ThreadWorker* worker, bool append2List)
{
    if(append2List)
        g_workersList.append(worker); // zapamietuje uchwyt do pozniejszego zatrzymania

    QThread* createdThred = new QThread(); // TODO: add this ???
    worker->moveToThread( createdThred );

    // connects the thread’s started() signal to the processing() slot in the worker, causing it to start.
    QObject::connect(createdThred, SIGNAL(started()), worker, SLOT(process()));
    // when the worker instance emits finished(), it will signal the thread to quit, i.e. shut down
    QObject::connect(worker, SIGNAL(finished()), createdThred, SLOT(quit()));
    // Finally, to prevent nasty crashes because the thread hasn’t fully shut down yet
    // when it is deleted, we connect the finished() of the thread (not the worker!)
    // to its own deleteLater() slot. This will cause the thread to be deleted only after
    // it has fully shut down.
    QObject::connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    QObject::connect(createdThred, SIGNAL(finished()), createdThred, SLOT(deleteLater()));
    //QObject::connect(createdThred, SIGNAL(finished()), this, SLOT(kurTest()));

    createdThred->start();
    createdThred->setObjectName( worker->metaObject()->className() );
}
/** Zatrzymuje wszystkie watki z wyjatkiem m_mainWindow (potrzebne dla rozwiazania z oknem "Please wait") */
void Framework::stop(bool majorChangesInCfg)
{
    qDebug() << "stop Threads, majorChangesInCfg: " << majorChangesInCfg;

    this->stopThreads();    // wszystkie z listy

 }

/** Zatrzymuje wszystkie utworzone watki z listy */
void Framework::stopThreads()
{
    int tempIter = 0;
    QList<ThreadWorker*>::iterator worker;
    for (worker = g_workersList.begin(); worker != g_workersList.end(); ++worker, tempIter++){

        this->stopThread(*worker);

        qDebug() << "Framework quit one worker, i = " << tempIter;
    }

    g_workersList.clear();
}

void Framework::stopThread(ThreadWorker *worker)
{
    worker->quit();
    bool finished = worker->thread()->wait(3000);
    if(!finished){
        qDebug() << "!!!!!!!!!!!!!!!!!!!!!!! Quit worker timeout !!!!";
        //Q_ASSERT(0);
    }
}

#ifndef TESTPROCESS_H
#define TESTPROCESS_H

#include <QObject>

#include "ThreadWorker.h"

class TestProcess : public ThreadWorker
{
    Q_OBJECT
public:
    explicit TestProcess(QObject *parent = 0);
    ~TestProcess();

    void quit(); // implementacja w klasie dziedziczacej

signals:
//void finished();
//void error(QString err);

public slots:
void process(); // implementacja w klasie dziedziczacej

private:


};

#endif // TESTPROCESS_H

/********************************************************************************
** Form generated from reading UI file 'infoform.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFOFORM_H
#define UI_INFOFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_InfoForm
{
public:
    QLabel *label;

    void setupUi(QWidget *InfoForm)
    {
        if (InfoForm->objectName().isEmpty())
            InfoForm->setObjectName(QString::fromUtf8("InfoForm"));
        InfoForm->resize(711, 356);
        InfoForm->setStyleSheet(QString::fromUtf8("QWidget{\n"
"	background-color: #e6e6e6;\n"
"}\n"
"\n"
"QLabel{\n"
"	font: 24pt Arial red;\n"
"	font-weight: bold;\n"
"	color: red;\n"
"	text-align: center;\n"
"}"));
        label = new QLabel(InfoForm);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 711, 351));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial red"));
        font.setPointSize(24);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        retranslateUi(InfoForm);

        QMetaObject::connectSlotsByName(InfoForm);
    } // setupUi

    void retranslateUi(QWidget *InfoForm)
    {
        InfoForm->setWindowTitle(QApplication::translate("InfoForm", "Form", nullptr));
        label->setText(QApplication::translate("InfoForm", "Prosz\304\231 czeka\304\207...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InfoForm: public Ui_InfoForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFOFORM_H

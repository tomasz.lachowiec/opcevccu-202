/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Program/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[20];
    char stringdata0[335];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "changePage"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 12), // "EPageIndexes"
QT_MOC_LITERAL(4, 36, 7), // "pageIdx"
QT_MOC_LITERAL(5, 44, 23), // "on_actionExit_triggered"
QT_MOC_LITERAL(6, 68, 24), // "on_consoleButton_toggled"
QT_MOC_LITERAL(7, 93, 7), // "checked"
QT_MOC_LITERAL(8, 101, 10), // "showMsgBox"
QT_MOC_LITERAL(9, 112, 3), // "msg"
QT_MOC_LITERAL(10, 116, 5), // "title"
QT_MOC_LITERAL(11, 122, 4), // "icon"
QT_MOC_LITERAL(12, 127, 17), // "addConsoleMessage"
QT_MOC_LITERAL(13, 145, 23), // "on_deviceButton_clicked"
QT_MOC_LITERAL(14, 169, 30), // "on_configurationButton_clicked"
QT_MOC_LITERAL(15, 200, 28), // "on_calibrationButton_clicked"
QT_MOC_LITERAL(16, 229, 23), // "on_ManualButton_clicked"
QT_MOC_LITERAL(17, 253, 24), // "on_actionSetup_triggered"
QT_MOC_LITERAL(18, 278, 26), // "on_actionConnect_triggered"
QT_MOC_LITERAL(19, 305, 29) // "on_actionDisconnect_triggered"

    },
    "MainWindow\0changePage\0\0EPageIndexes\0"
    "pageIdx\0on_actionExit_triggered\0"
    "on_consoleButton_toggled\0checked\0"
    "showMsgBox\0msg\0title\0icon\0addConsoleMessage\0"
    "on_deviceButton_clicked\0"
    "on_configurationButton_clicked\0"
    "on_calibrationButton_clicked\0"
    "on_ManualButton_clicked\0"
    "on_actionSetup_triggered\0"
    "on_actionConnect_triggered\0"
    "on_actionDisconnect_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x0a /* Public */,
       5,    0,   92,    2, 0x08 /* Private */,
       6,    1,   93,    2, 0x08 /* Private */,
       8,    3,   96,    2, 0x08 /* Private */,
       8,    2,  103,    2, 0x28 /* Private | MethodCloned */,
       8,    1,  108,    2, 0x28 /* Private | MethodCloned */,
      12,    2,  111,    2, 0x08 /* Private */,
      12,    1,  116,    2, 0x28 /* Private | MethodCloned */,
      13,    0,  119,    2, 0x08 /* Private */,
      14,    0,  120,    2, 0x08 /* Private */,
      15,    0,  121,    2, 0x08 /* Private */,
      16,    0,  122,    2, 0x08 /* Private */,
      17,    0,  123,    2, 0x08 /* Private */,
      18,    0,  124,    2, 0x08 /* Private */,
      19,    0,  125,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,    9,   10,   11,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    9,   10,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    9,   11,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->changePage((*reinterpret_cast< EPageIndexes(*)>(_a[1]))); break;
        case 1: _t->on_actionExit_triggered(); break;
        case 2: _t->on_consoleButton_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->showMsgBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->showMsgBox((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->showMsgBox((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->addConsoleMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->addConsoleMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->on_deviceButton_clicked(); break;
        case 9: _t->on_configurationButton_clicked(); break;
        case 10: _t->on_calibrationButton_clicked(); break;
        case 11: _t->on_ManualButton_clicked(); break;
        case 12: _t->on_actionSetup_triggered(); break;
        case 13: _t->on_actionConnect_triggered(); break;
        case 14: _t->on_actionDisconnect_triggered(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'calibrationpage.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Program/GUI/Pages/Modules/calibrationpage.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'calibrationpage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CalibrationPage_t {
    QByteArrayData data[25];
    char stringdata0[514];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CalibrationPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CalibrationPage_t qt_meta_stringdata_CalibrationPage = {
    {
QT_MOC_LITERAL(0, 0, 15), // "CalibrationPage"
QT_MOC_LITERAL(1, 16, 17), // "on_bCalib_clicked"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 23), // "on_btemp1_setP1_clicked"
QT_MOC_LITERAL(4, 59, 23), // "on_btemp1_setP2_clicked"
QT_MOC_LITERAL(5, 83, 25), // "on_btemp1_setP1_2_clicked"
QT_MOC_LITERAL(6, 109, 25), // "on_btemp1_setP2_2_clicked"
QT_MOC_LITERAL(7, 135, 23), // "on_btemp2_setP1_clicked"
QT_MOC_LITERAL(8, 159, 23), // "on_btemp2_setP2_clicked"
QT_MOC_LITERAL(9, 183, 15), // "connectToDevice"
QT_MOC_LITERAL(10, 199, 16), // "on_bDisc_clicked"
QT_MOC_LITERAL(11, 216, 22), // "on_bCalibTemp1_clicked"
QT_MOC_LITERAL(12, 239, 22), // "on_bCalibTemp2_clicked"
QT_MOC_LITERAL(13, 262, 19), // "on_bCalibCP_clicked"
QT_MOC_LITERAL(14, 282, 25), // "on_bCalibCP_Minus_clicked"
QT_MOC_LITERAL(15, 308, 23), // "on_bTemp2_setP2_clicked"
QT_MOC_LITERAL(16, 332, 25), // "on_btemp1_setPMin_clicked"
QT_MOC_LITERAL(17, 358, 25), // "on_btemp1_setPMax_clicked"
QT_MOC_LITERAL(18, 384, 25), // "on_btemp1_setPkt2_clicked"
QT_MOC_LITERAL(19, 410, 9), // "timerTick"
QT_MOC_LITERAL(20, 420, 14), // "createInfoForm"
QT_MOC_LITERAL(21, 435, 13), // "measureValues"
QT_MOC_LITERAL(22, 449, 19), // "on_bMeausre_clicked"
QT_MOC_LITERAL(23, 469, 21), // "on_bMeausre_2_clicked"
QT_MOC_LITERAL(24, 491, 22) // "on_bCreatePlot_clicked"

    },
    "CalibrationPage\0on_bCalib_clicked\0\0"
    "on_btemp1_setP1_clicked\0on_btemp1_setP2_clicked\0"
    "on_btemp1_setP1_2_clicked\0"
    "on_btemp1_setP2_2_clicked\0"
    "on_btemp2_setP1_clicked\0on_btemp2_setP2_clicked\0"
    "connectToDevice\0on_bDisc_clicked\0"
    "on_bCalibTemp1_clicked\0on_bCalibTemp2_clicked\0"
    "on_bCalibCP_clicked\0on_bCalibCP_Minus_clicked\0"
    "on_bTemp2_setP2_clicked\0"
    "on_btemp1_setPMin_clicked\0"
    "on_btemp1_setPMax_clicked\0"
    "on_btemp1_setPkt2_clicked\0timerTick\0"
    "createInfoForm\0measureValues\0"
    "on_bMeausre_clicked\0on_bMeausre_2_clicked\0"
    "on_bCreatePlot_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CalibrationPage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x08 /* Private */,
       3,    0,  130,    2, 0x08 /* Private */,
       4,    0,  131,    2, 0x08 /* Private */,
       5,    0,  132,    2, 0x08 /* Private */,
       6,    0,  133,    2, 0x08 /* Private */,
       7,    0,  134,    2, 0x08 /* Private */,
       8,    0,  135,    2, 0x08 /* Private */,
       9,    0,  136,    2, 0x08 /* Private */,
      10,    0,  137,    2, 0x08 /* Private */,
      11,    0,  138,    2, 0x08 /* Private */,
      12,    0,  139,    2, 0x08 /* Private */,
      13,    0,  140,    2, 0x08 /* Private */,
      14,    0,  141,    2, 0x08 /* Private */,
      15,    0,  142,    2, 0x08 /* Private */,
      16,    0,  143,    2, 0x08 /* Private */,
      17,    0,  144,    2, 0x08 /* Private */,
      18,    0,  145,    2, 0x08 /* Private */,
      19,    0,  146,    2, 0x08 /* Private */,
      20,    0,  147,    2, 0x08 /* Private */,
      21,    0,  148,    2, 0x08 /* Private */,
      22,    0,  149,    2, 0x08 /* Private */,
      23,    0,  150,    2, 0x08 /* Private */,
      24,    0,  151,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CalibrationPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CalibrationPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_bCalib_clicked(); break;
        case 1: _t->on_btemp1_setP1_clicked(); break;
        case 2: _t->on_btemp1_setP2_clicked(); break;
        case 3: _t->on_btemp1_setP1_2_clicked(); break;
        case 4: _t->on_btemp1_setP2_2_clicked(); break;
        case 5: _t->on_btemp2_setP1_clicked(); break;
        case 6: _t->on_btemp2_setP2_clicked(); break;
        case 7: { bool _r = _t->connectToDevice();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: _t->on_bDisc_clicked(); break;
        case 9: _t->on_bCalibTemp1_clicked(); break;
        case 10: _t->on_bCalibTemp2_clicked(); break;
        case 11: _t->on_bCalibCP_clicked(); break;
        case 12: _t->on_bCalibCP_Minus_clicked(); break;
        case 13: _t->on_bTemp2_setP2_clicked(); break;
        case 14: _t->on_btemp1_setPMin_clicked(); break;
        case 15: _t->on_btemp1_setPMax_clicked(); break;
        case 16: _t->on_btemp1_setPkt2_clicked(); break;
        case 17: _t->timerTick(); break;
        case 18: _t->createInfoForm(); break;
        case 19: _t->measureValues(); break;
        case 20: _t->on_bMeausre_clicked(); break;
        case 21: _t->on_bMeausre_2_clicked(); break;
        case 22: _t->on_bCreatePlot_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CalibrationPage::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_CalibrationPage.data,
    qt_meta_data_CalibrationPage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CalibrationPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CalibrationPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CalibrationPage.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int CalibrationPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

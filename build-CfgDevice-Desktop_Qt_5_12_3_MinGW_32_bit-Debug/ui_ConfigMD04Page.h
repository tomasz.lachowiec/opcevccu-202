/********************************************************************************
** Form generated from reading UI file 'ConfigMD04Page.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGMD04PAGE_H
#define UI_CONFIGMD04PAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigMD04Page
{
public:
    QTabWidget *tab_configDevice;
    QWidget *tab_InterfaceConfig;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_8;
    QGridLayout *gridLayout;
    QLineEdit *ipAddress;
    QSpacerItem *verticalSpacer;
    QLineEdit *netMask;
    QCheckBox *enableDHCP;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *ipGateway;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;
    QLabel *label_7;
    QGridLayout *gridLayout_2;
    QLabel *label_5;
    QLabel *label_6;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *modbusPort;
    QPushButton *readCommunicationConfigButton;
    QLineEdit *modbusID;
    QPushButton *writeCommunicationConfigButton;
    QPushButton *resetDeviceButton;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tab_InputOutputConfig;

    void setupUi(QDialog *ConfigMD04Page)
    {
        if (ConfigMD04Page->objectName().isEmpty())
            ConfigMD04Page->setObjectName(QString::fromUtf8("ConfigMD04Page"));
        ConfigMD04Page->resize(791, 500);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ConfigMD04Page->sizePolicy().hasHeightForWidth());
        ConfigMD04Page->setSizePolicy(sizePolicy);
        ConfigMD04Page->setMinimumSize(QSize(791, 0));
        ConfigMD04Page->setStyleSheet(QString::fromUtf8("background-color:white;"));
        tab_configDevice = new QTabWidget(ConfigMD04Page);
        tab_configDevice->setObjectName(QString::fromUtf8("tab_configDevice"));
        tab_configDevice->setEnabled(true);
        tab_configDevice->setGeometry(QRect(0, 0, 791, 490));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tab_configDevice->sizePolicy().hasHeightForWidth());
        tab_configDevice->setSizePolicy(sizePolicy1);
        tab_configDevice->setAutoFillBackground(false);
        tab_configDevice->setStyleSheet(QString::fromUtf8("QTabBar::tab:hover {\n"
" \n"
"	background-color:#1C7ECC;\n"
"	border:none;\n"
"	color:white;\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
" \n"
"	background-color: rgb(22, 147, 204);\n"
"	border:none;\n"
"	color:white;\n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"    margin-top: 6px; /* make non-selected tabs look smaller */\n"
"}\n"
"\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 2px solid #C4C4C3;\n"
"    border-bottom-color: #C2C7CB; /* same as the pane color */\n"
"    border-top-left-radius: 4px;\n"
"    border-top-right-radius: 4px;\n"
"    min-width: 8ex;\n"
"    padding: 2px;\n"
"}\n"
"\n"
"\n"
"#tab_configDevice\n"
"{\n"
"\n"
"	QToolButton {background-color: rgb(193, 215, 255);} 			 \n"
"    QToolButton:pressed {background-color: rgb(142, 173, 197);}s\n"
"	\n"
"font: 75 10pt \"MS Shell Dlg 2\";\n"
"}"));
        tab_InterfaceConfig = new QWidget();
        tab_InterfaceConfig->setObjectName(QString::fromUtf8("tab_InterfaceConfig"));
        tab_InterfaceConfig->setStyleSheet(QString::fromUtf8("background-color: rgb(229, 246, 255)\n"
"\n"
""));
        verticalLayoutWidget = new QWidget(tab_InterfaceConfig);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 647, 440));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(verticalLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font;
        font.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font.setPointSize(14);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        label_8->setFont(font);
        label_8->setStyleSheet(QString::fromUtf8("font: 75 14pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(label_8);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        ipAddress = new QLineEdit(verticalLayoutWidget);
        ipAddress->setObjectName(QString::fromUtf8("ipAddress"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(ipAddress->sizePolicy().hasHeightForWidth());
        ipAddress->setSizePolicy(sizePolicy2);
        ipAddress->setMinimumSize(QSize(140, 0));
        QFont font1;
        font1.setPointSize(14);
        ipAddress->setFont(font1);
        ipAddress->setStyleSheet(QString::fromUtf8("background-color:white;"));

        gridLayout->addWidget(ipAddress, 1, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 4, 0, 1, 1);

        netMask = new QLineEdit(verticalLayoutWidget);
        netMask->setObjectName(QString::fromUtf8("netMask"));
        sizePolicy2.setHeightForWidth(netMask->sizePolicy().hasHeightForWidth());
        netMask->setSizePolicy(sizePolicy2);
        netMask->setMinimumSize(QSize(140, 0));
        netMask->setFont(font1);
        netMask->setStyleSheet(QString::fromUtf8("background-color:white;"));

        gridLayout->addWidget(netMask, 2, 1, 1, 1);

        enableDHCP = new QCheckBox(verticalLayoutWidget);
        enableDHCP->setObjectName(QString::fromUtf8("enableDHCP"));

        gridLayout->addWidget(enableDHCP, 0, 1, 1, 1);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        ipGateway = new QLineEdit(verticalLayoutWidget);
        ipGateway->setObjectName(QString::fromUtf8("ipGateway"));
        sizePolicy2.setHeightForWidth(ipGateway->sizePolicy().hasHeightForWidth());
        ipGateway->setSizePolicy(sizePolicy2);
        ipGateway->setMinimumSize(QSize(140, 0));
        ipGateway->setFont(font1);
        ipGateway->setStyleSheet(QString::fromUtf8("background-color:white;"));

        gridLayout->addWidget(ipGateway, 3, 1, 1, 1);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        label_7 = new QLabel(verticalLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);
        label_7->setStyleSheet(QString::fromUtf8("font: 75 14pt \"MS Shell Dlg 2\";"));

        verticalLayout->addWidget(label_7);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);

        label_6 = new QLabel(verticalLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        gridLayout_2->addWidget(label_6, 1, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 3, 0, 1, 1);

        modbusPort = new QLineEdit(verticalLayoutWidget);
        modbusPort->setObjectName(QString::fromUtf8("modbusPort"));
        sizePolicy2.setHeightForWidth(modbusPort->sizePolicy().hasHeightForWidth());
        modbusPort->setSizePolicy(sizePolicy2);
        modbusPort->setMinimumSize(QSize(40, 0));
        modbusPort->setFont(font1);
        modbusPort->setStyleSheet(QString::fromUtf8("background-color:white;"));

        gridLayout_2->addWidget(modbusPort, 1, 1, 1, 1);

        readCommunicationConfigButton = new QPushButton(verticalLayoutWidget);
        readCommunicationConfigButton->setObjectName(QString::fromUtf8("readCommunicationConfigButton"));
        sizePolicy2.setHeightForWidth(readCommunicationConfigButton->sizePolicy().hasHeightForWidth());
        readCommunicationConfigButton->setSizePolicy(sizePolicy2);
        readCommunicationConfigButton->setMinimumSize(QSize(150, 60));
        readCommunicationConfigButton->setCursor(QCursor(Qt::PointingHandCursor));
        readCommunicationConfigButton->setStyleSheet(QString::fromUtf8("#readCommunicationConfigButton\n"
"{\n"
"	color:#FCFCFC;\n"
"	background-color: rgb(76, 129, 85);\n"
"	border:none;\n"
"	font: 10pt \"Microsoft YaHei UI\";\n"
"	font-weight:bold;\n"
"}\n"
"\n"
"#readCommunicationConfigButton:hover\n"
"{\n"
"	background-color:#1C7ECC;\n"
"	border:none;\n"
"	border-radius:5px;\n"
"	color:white;\n"
"}"));

        gridLayout_2->addWidget(readCommunicationConfigButton, 3, 2, 1, 1);

        modbusID = new QLineEdit(verticalLayoutWidget);
        modbusID->setObjectName(QString::fromUtf8("modbusID"));
        sizePolicy2.setHeightForWidth(modbusID->sizePolicy().hasHeightForWidth());
        modbusID->setSizePolicy(sizePolicy2);
        modbusID->setMinimumSize(QSize(50, 0));
        modbusID->setFont(font1);
        modbusID->setStyleSheet(QString::fromUtf8("background-color:white;"));

        gridLayout_2->addWidget(modbusID, 0, 1, 1, 1);

        writeCommunicationConfigButton = new QPushButton(verticalLayoutWidget);
        writeCommunicationConfigButton->setObjectName(QString::fromUtf8("writeCommunicationConfigButton"));
        sizePolicy2.setHeightForWidth(writeCommunicationConfigButton->sizePolicy().hasHeightForWidth());
        writeCommunicationConfigButton->setSizePolicy(sizePolicy2);
        writeCommunicationConfigButton->setMinimumSize(QSize(150, 60));
        writeCommunicationConfigButton->setCursor(QCursor(Qt::PointingHandCursor));
        writeCommunicationConfigButton->setStyleSheet(QString::fromUtf8("#writeCommunicationConfigButton\n"
"{\n"
"	color:#FCFCFC;\n"
"	background-color: rgb(76, 129, 85);\n"
"	border:none;\n"
"	font: 10pt \"Microsoft YaHei UI\";\n"
"	font-weight:bold;\n"
"}\n"
"\n"
"#writeCommunicationConfigButton:hover\n"
"{\n"
"	background-color:#1C7ECC;\n"
"	border:none;\n"
"	border-radius:5px;\n"
"	color:white;\n"
"}"));

        gridLayout_2->addWidget(writeCommunicationConfigButton, 3, 3, 1, 1);

        resetDeviceButton = new QPushButton(verticalLayoutWidget);
        resetDeviceButton->setObjectName(QString::fromUtf8("resetDeviceButton"));
        sizePolicy2.setHeightForWidth(resetDeviceButton->sizePolicy().hasHeightForWidth());
        resetDeviceButton->setSizePolicy(sizePolicy2);
        resetDeviceButton->setMinimumSize(QSize(150, 60));
        resetDeviceButton->setCursor(QCursor(Qt::PointingHandCursor));
        resetDeviceButton->setStyleSheet(QString::fromUtf8("#resetDeviceButton\n"
"{\n"
"	color:#FCFCFC;\n"
"	background-color: rgb(76, 129, 85);\n"
"	border:none;\n"
"	font: 10pt \"Microsoft YaHei UI\";\n"
"	font-weight:bold;\n"
"}\n"
"\n"
"#resetDeviceButton:hover\n"
"{\n"
"	background-color:#1C7ECC;\n"
"	border:none;\n"
"	border-radius:5px;\n"
"	color:white;\n"
"}"));

        gridLayout_2->addWidget(resetDeviceButton, 3, 4, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 3, 5, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        tab_configDevice->addTab(tab_InterfaceConfig, QString());
        tab_InputOutputConfig = new QWidget();
        tab_InputOutputConfig->setObjectName(QString::fromUtf8("tab_InputOutputConfig"));
        tab_configDevice->addTab(tab_InputOutputConfig, QString());

        retranslateUi(ConfigMD04Page);

        tab_configDevice->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ConfigMD04Page);
    } // setupUi

    void retranslateUi(QDialog *ConfigMD04Page)
    {
        ConfigMD04Page->setWindowTitle(QApplication::translate("ConfigMD04Page", "Dialog", nullptr));
        label_8->setText(QApplication::translate("ConfigMD04Page", "Ethernet configuration", nullptr));
        enableDHCP->setText(QString());
        label_3->setText(QApplication::translate("ConfigMD04Page", "Net mask:", nullptr));
        label_2->setText(QApplication::translate("ConfigMD04Page", "IP address:", nullptr));
        label->setText(QApplication::translate("ConfigMD04Page", "DHCP Enable:", nullptr));
        label_4->setText(QApplication::translate("ConfigMD04Page", "Gateway address:", nullptr));
        label_7->setText(QApplication::translate("ConfigMD04Page", "Modbus TCP configuration", nullptr));
        label_5->setText(QApplication::translate("ConfigMD04Page", "Modbus ID:", nullptr));
        label_6->setText(QApplication::translate("ConfigMD04Page", "Modbus port:", nullptr));
        readCommunicationConfigButton->setText(QApplication::translate("ConfigMD04Page", "Read configuration", nullptr));
        writeCommunicationConfigButton->setText(QApplication::translate("ConfigMD04Page", "Write configuration", nullptr));
        resetDeviceButton->setText(QApplication::translate("ConfigMD04Page", "Reset device", nullptr));
        tab_configDevice->setTabText(tab_configDevice->indexOf(tab_InterfaceConfig), QApplication::translate("ConfigMD04Page", "Interfaces", nullptr));
        tab_configDevice->setTabText(tab_configDevice->indexOf(tab_InputOutputConfig), QApplication::translate("ConfigMD04Page", "Input/Output", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ConfigMD04Page: public Ui_ConfigMD04Page {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGMD04PAGE_H

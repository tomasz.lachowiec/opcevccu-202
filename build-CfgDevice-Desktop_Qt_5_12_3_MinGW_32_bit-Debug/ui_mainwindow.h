/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen_project;
    QAction *actionSave_project;
    QAction *actionSave_project_as;
    QAction *actionClose_project;
    QAction *actionExit;
    QAction *actionNew_project;
    QAction *actionSetup;
    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionDevice;
    QAction *actionRead_configuration;
    QAction *actionWrite_configuration;
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QFrame *rightWrapper;
    QGridLayout *gridLayout_4;
    QFrame *horizontalFrame;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *consoleButton;
    QLabel *statusBarLabel;
    QSplitter *splitter;
    QStackedWidget *stackedWidget;
    QFrame *consoleFrame;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_5;
    QFrame *leftToolbarFrame;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QToolButton *deviceButton;
    QToolButton *calibrationButton;
    QToolButton *configurationButton;
    QToolButton *ManualButton;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *menu;
    QSpacerItem *horizontalSpacer;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuTools;
    QMenu *menuConfiguratio;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1000, 850);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(1000, 850));
        MainWindow->setMaximumSize(QSize(1000, 850));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(238, 238, 238, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(246, 246, 246, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(119, 119, 119, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(159, 159, 159, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush2);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush3);
        QBrush brush6(QColor(255, 255, 220, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush7(QColor(0, 0, 0, 128));
        brush7.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush7);
#endif
        MainWindow->setPalette(palette);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        MainWindow->setFont(font);
        MainWindow->setStyleSheet(QString::fromUtf8("* {\n"
"font: 10pt \"DejaVu Sans\";\n"
"}\n"
"\n"
".commonMenuButton\n"
"{\n"
"text-align:center;\n"
"color:white;\n"
"border:0px solid #dcdcdc;\n"
"background-color:#1C91D1;\n"
"border-radius:5px;\n"
"font: 10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"min-height: 40px;\n"
"}\n"
"\n"
".commonMenuButton:hover\n"
"{\n"
"background-color:rgb(17,158,90);\n"
"}\n"
"\n"
".commonMenuButtonAbort\n"
"{\n"
"text-align:center;\n"
"color:white;\n"
"border:0px solid #dcdcdc;\n"
"background-color:#1C91D1;\n"
"border-radius:5px;\n"
"font: 10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"}\n"
"\n"
".commonMenuButtonAbort:hover\n"
"{\n"
"background-color:rgb(244, 244, 244);\n"
"}\n"
"\n"
".commonMenuButton:disabled\n"
"{\n"
"background-color:gray;\n"
"}\n"
"\n"
".commonMenuButtonAbort:disabled\n"
"{\n"
"background-color:gray;\n"
"}"));
        actionOpen_project = new QAction(MainWindow);
        actionOpen_project->setObjectName(QString::fromUtf8("actionOpen_project"));
        actionSave_project = new QAction(MainWindow);
        actionSave_project->setObjectName(QString::fromUtf8("actionSave_project"));
        actionSave_project_as = new QAction(MainWindow);
        actionSave_project_as->setObjectName(QString::fromUtf8("actionSave_project_as"));
        actionClose_project = new QAction(MainWindow);
        actionClose_project->setObjectName(QString::fromUtf8("actionClose_project"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/_resources/icons/buttons/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon);
        actionNew_project = new QAction(MainWindow);
        actionNew_project->setObjectName(QString::fromUtf8("actionNew_project"));
        actionSetup = new QAction(MainWindow);
        actionSetup->setObjectName(QString::fromUtf8("actionSetup"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/_resources/icons/buttons/setup.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSetup->setIcon(icon1);
        actionConnect = new QAction(MainWindow);
        actionConnect->setObjectName(QString::fromUtf8("actionConnect"));
        actionConnect->setEnabled(true);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/_resources/icons/buttons/connect.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConnect->setIcon(icon2);
        actionDisconnect = new QAction(MainWindow);
        actionDisconnect->setObjectName(QString::fromUtf8("actionDisconnect"));
        actionDisconnect->setEnabled(false);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/_resources/icons/buttons/disconnected.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDisconnect->setIcon(icon3);
        actionDevice = new QAction(MainWindow);
        actionDevice->setObjectName(QString::fromUtf8("actionDevice"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/_resources/icons/buttons/device.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDevice->setIcon(icon4);
        actionRead_configuration = new QAction(MainWindow);
        actionRead_configuration->setObjectName(QString::fromUtf8("actionRead_configuration"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/_resources/icons/buttons/upload.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRead_configuration->setIcon(icon5);
        actionWrite_configuration = new QAction(MainWindow);
        actionWrite_configuration->setObjectName(QString::fromUtf8("actionWrite_configuration"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/_resources/icons/buttons/download.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionWrite_configuration->setIcon(icon6);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("/*#centralWidget\n"
"{\n"
"	background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(111, 113, 120, 255), stop:1 rgba(69, 70, 75, 255));\n"
"}*/\n"
"background-color: rgba(69, 70, 75, 255);"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        rightWrapper = new QFrame(centralWidget);
        rightWrapper->setObjectName(QString::fromUtf8("rightWrapper"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(rightWrapper->sizePolicy().hasHeightForWidth());
        rightWrapper->setSizePolicy(sizePolicy1);
        rightWrapper->setMinimumSize(QSize(800, 0));
        gridLayout_4 = new QGridLayout(rightWrapper);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalFrame = new QFrame(rightWrapper);
        horizontalFrame->setObjectName(QString::fromUtf8("horizontalFrame"));
        horizontalFrame->setMinimumSize(QSize(0, 36));
        horizontalLayout_2 = new QHBoxLayout(horizontalFrame);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        consoleButton = new QToolButton(horizontalFrame);
        consoleButton->setObjectName(QString::fromUtf8("consoleButton"));
        consoleButton->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(consoleButton->sizePolicy().hasHeightForWidth());
        consoleButton->setSizePolicy(sizePolicy2);
        consoleButton->setMinimumSize(QSize(0, 0));
        consoleButton->setMaximumSize(QSize(16777215, 16777215));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        consoleButton->setFont(font1);
        consoleButton->setCursor(QCursor(Qt::PointingHandCursor));
        consoleButton->setStyleSheet(QString::fromUtf8("#consoleButton\n"
"{\n"
"background-color: rgba(255, 255, 255, 0); \n"
"color:rgb(17,158,90);\n"
"font:10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"margin:0;\n"
"padding:0;\n"
"}\n"
"\n"
"#consoleButton:checked\n"
"{\n"
"background-color: rgba(255, 255, 255, 0); \n"
"color:#CC4F2A;\n"
"font:10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"margin:0;\n"
"padding:0;\n"
"}"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icons/_resources/icons/console.png"), QSize(), QIcon::Normal, QIcon::Off);
        consoleButton->setIcon(icon7);
        consoleButton->setIconSize(QSize(26, 26));
        consoleButton->setCheckable(true);
        consoleButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        consoleButton->setAutoRaise(false);

        horizontalLayout_3->addWidget(consoleButton);

        statusBarLabel = new QLabel(horizontalFrame);
        statusBarLabel->setObjectName(QString::fromUtf8("statusBarLabel"));
        QSizePolicy sizePolicy3(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(statusBarLabel->sizePolicy().hasHeightForWidth());
        statusBarLabel->setSizePolicy(sizePolicy3);
        statusBarLabel->setFont(font1);
        statusBarLabel->setLayoutDirection(Qt::LeftToRight);
        statusBarLabel->setStyleSheet(QString::fromUtf8("background-color:transparent;\n"
"color:#FCFCFC;\n"
"font: 10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"margin-right:2px;"));
        statusBarLabel->setText(QString::fromUtf8("Copyright \302\251 2017"));
        statusBarLabel->setTextFormat(Qt::AutoText);
        statusBarLabel->setScaledContents(true);
        statusBarLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        statusBarLabel->setMargin(0);

        horizontalLayout_3->addWidget(statusBarLabel);


        horizontalLayout_2->addLayout(horizontalLayout_3);


        gridLayout_4->addWidget(horizontalFrame, 1, 0, 1, 1);

        splitter = new QSplitter(rightWrapper);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        splitter->setOpaqueResize(false);
        splitter->setHandleWidth(2);
        stackedWidget = new QStackedWidget(splitter);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setMinimumSize(QSize(0, 200));
        QPalette palette1;
        QBrush brush8(QColor(69, 70, 75, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush8);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush8);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush8);
        stackedWidget->setPalette(palette1);
        splitter->addWidget(stackedWidget);
        consoleFrame = new QFrame(splitter);
        consoleFrame->setObjectName(QString::fromUtf8("consoleFrame"));
        consoleFrame->setMinimumSize(QSize(0, 50));
        consoleFrame->setMaximumSize(QSize(16777215, 200));
        gridLayout = new QGridLayout(consoleFrame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));

        gridLayout->addLayout(gridLayout_5, 0, 0, 1, 1);

        splitter->addWidget(consoleFrame);

        gridLayout_4->addWidget(splitter, 0, 0, 1, 1);


        gridLayout_2->addWidget(rightWrapper, 0, 1, 1, 1);

        leftToolbarFrame = new QFrame(centralWidget);
        leftToolbarFrame->setObjectName(QString::fromUtf8("leftToolbarFrame"));
        sizePolicy1.setHeightForWidth(leftToolbarFrame->sizePolicy().hasHeightForWidth());
        leftToolbarFrame->setSizePolicy(sizePolicy1);
        leftToolbarFrame->setMinimumSize(QSize(160, 0));
        leftToolbarFrame->setMaximumSize(QSize(170, 16777215));
        leftToolbarFrame->setStyleSheet(QString::fromUtf8("#leftToolbarFrame\n"
"{ \n"
"	background-color: transparent;\n"
"}\n"
"\n"
"/*nadpisywane w MainWindow, zostawione tylko dla podgl\304\205du GUI*/\n"
".toolButton\n"
"{\n"
"color:#FCFCFC;\n"
"/*background-color: transparent;*/\n"
"	\n"
"	background-color: rgb(124, 157, 117);\n"
"border:none;\n"
"font: 10pt \"Microsoft YaHei UI\";\n"
"font-weight:bold;\n"
"}\n"
"\n"
".toolButton:hover\n"
"{\n"
"background-color:#1C7ECC;\n"
" border:none;\n"
" border-radius:5px;\n"
"color:white;\n"
"}\n"
"\n"
".toolButton:disabled\n"
"{\n"
"color:gray\n"
"/*background-color: rgba(255, 255, 255, 0); */\n"
"}"));
        verticalLayout = new QVBoxLayout(leftToolbarFrame);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        deviceButton = new QToolButton(leftToolbarFrame);
        deviceButton->setObjectName(QString::fromUtf8("deviceButton"));
        sizePolicy3.setHeightForWidth(deviceButton->sizePolicy().hasHeightForWidth());
        deviceButton->setSizePolicy(sizePolicy3);
        deviceButton->setMinimumSize(QSize(160, 50));
        deviceButton->setFont(font1);
        deviceButton->setCursor(QCursor(Qt::PointingHandCursor));
        deviceButton->setStyleSheet(QString::fromUtf8(""));
        deviceButton->setIconSize(QSize(24, 24));
        deviceButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        deviceButton->setProperty("class", QVariant(QString::fromUtf8("toolButton")));

        verticalLayout_2->addWidget(deviceButton);

        calibrationButton = new QToolButton(leftToolbarFrame);
        calibrationButton->setObjectName(QString::fromUtf8("calibrationButton"));
        calibrationButton->setEnabled(false);
        QSizePolicy sizePolicy4(QSizePolicy::Ignored, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(calibrationButton->sizePolicy().hasHeightForWidth());
        calibrationButton->setSizePolicy(sizePolicy4);
        calibrationButton->setMinimumSize(QSize(160, 50));
        calibrationButton->setFont(font1);
        calibrationButton->setCursor(QCursor(Qt::PointingHandCursor));
        calibrationButton->setStyleSheet(QString::fromUtf8(""));
        calibrationButton->setIconSize(QSize(24, 24));
        calibrationButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        calibrationButton->setProperty("class", QVariant(QString::fromUtf8("toolButton")));

        verticalLayout_2->addWidget(calibrationButton);

        configurationButton = new QToolButton(leftToolbarFrame);
        configurationButton->setObjectName(QString::fromUtf8("configurationButton"));
        configurationButton->setEnabled(false);
        sizePolicy4.setHeightForWidth(configurationButton->sizePolicy().hasHeightForWidth());
        configurationButton->setSizePolicy(sizePolicy4);
        configurationButton->setMinimumSize(QSize(160, 50));
        configurationButton->setFont(font1);
        configurationButton->setCursor(QCursor(Qt::PointingHandCursor));
        configurationButton->setLayoutDirection(Qt::LeftToRight);
        configurationButton->setStyleSheet(QString::fromUtf8(""));
        configurationButton->setIconSize(QSize(24, 24));
        configurationButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        configurationButton->setProperty("class", QVariant(QString::fromUtf8("toolButton")));

        verticalLayout_2->addWidget(configurationButton);

        ManualButton = new QToolButton(leftToolbarFrame);
        ManualButton->setObjectName(QString::fromUtf8("ManualButton"));
        ManualButton->setEnabled(false);
        sizePolicy4.setHeightForWidth(ManualButton->sizePolicy().hasHeightForWidth());
        ManualButton->setSizePolicy(sizePolicy4);
        ManualButton->setMinimumSize(QSize(160, 50));
        ManualButton->setFont(font1);
        ManualButton->setCursor(QCursor(Qt::PointingHandCursor));
        ManualButton->setStyleSheet(QString::fromUtf8(""));
        ManualButton->setIconSize(QSize(24, 24));
        ManualButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        ManualButton->setProperty("class", QVariant(QString::fromUtf8("toolButton")));

        verticalLayout_2->addWidget(ManualButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addLayout(verticalLayout_2);


        gridLayout_2->addWidget(leftToolbarFrame, 0, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 3, 0, 1, 1);

        menu = new QHBoxLayout();
        menu->setSpacing(6);
        menu->setObjectName(QString::fromUtf8("menu"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        menu->addItem(horizontalSpacer);


        gridLayout_3->addLayout(menu, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1000, 20));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuConfiguratio = new QMenu(menuBar);
        menuConfiguratio->setObjectName(QString::fromUtf8("menuConfiguratio"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuConfiguratio->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuTools->addAction(actionSetup);
        menuTools->addSeparator();
        menuTools->addAction(actionConnect);
        menuTools->addAction(actionDisconnect);
        menuConfiguratio->addAction(actionDevice);
        menuConfiguratio->addSeparator();
        menuConfiguratio->addAction(actionRead_configuration);
        menuConfiguratio->addAction(actionWrite_configuration);
        toolBar->addAction(actionDevice);
        toolBar->addAction(actionRead_configuration);
        toolBar->addAction(actionWrite_configuration);
        toolBar->addSeparator();
        toolBar->addAction(actionSetup);
        toolBar->addAction(actionConnect);
        toolBar->addAction(actionDisconnect);
        toolBar->addSeparator();
        toolBar->addAction(actionExit);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Configure software", nullptr));
        actionOpen_project->setText(QApplication::translate("MainWindow", "&Open project", nullptr));
        actionSave_project->setText(QApplication::translate("MainWindow", "&Save project", nullptr));
        actionSave_project_as->setText(QApplication::translate("MainWindow", "Save &project as", nullptr));
        actionClose_project->setText(QApplication::translate("MainWindow", "&Close project", nullptr));
        actionExit->setText(QApplication::translate("MainWindow", "&Exit", nullptr));
        actionNew_project->setText(QApplication::translate("MainWindow", "New project", nullptr));
        actionSetup->setText(QApplication::translate("MainWindow", "Setup", nullptr));
        actionConnect->setText(QApplication::translate("MainWindow", "Connect", nullptr));
        actionDisconnect->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        actionDevice->setText(QApplication::translate("MainWindow", "Device", nullptr));
        actionRead_configuration->setText(QApplication::translate("MainWindow", "Read configuration", nullptr));
        actionWrite_configuration->setText(QApplication::translate("MainWindow", "Write configuration", nullptr));
#ifndef QT_NO_TOOLTIP
        consoleButton->setToolTip(QApplication::translate("MainWindow", "Console", nullptr));
#endif // QT_NO_TOOLTIP
        consoleButton->setText(QApplication::translate("MainWindow", "SHOW CONSOLE", nullptr));
        deviceButton->setText(QApplication::translate("MainWindow", "DEVICE", nullptr));
        calibrationButton->setText(QApplication::translate("MainWindow", "CALIBRATION", nullptr));
        configurationButton->setText(QApplication::translate("MainWindow", "  CONFIGURATION", nullptr));
        ManualButton->setText(QApplication::translate("MainWindow", "MANUAL\n"
" FUNCIONS", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "&File", nullptr));
        menuTools->setTitle(QApplication::translate("MainWindow", "Tools", nullptr));
        menuConfiguratio->setTitle(QApplication::translate("MainWindow", "Configuration", nullptr));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

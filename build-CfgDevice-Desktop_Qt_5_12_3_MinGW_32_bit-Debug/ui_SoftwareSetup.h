/********************************************************************************
** Form generated from reading UI file 'SoftwareSetup.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOFTWARESETUP_H
#define UI_SOFTWARESETUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SoftwareSetup
{
public:
    QDialogButtonBox *buttonBox;
    QTabWidget *tabWidget;
    QWidget *interfaceSerial;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_7;
    QLabel *label_69;
    QComboBox *baudCOM;
    QLabel *label_70;
    QLabel *label_71;
    QComboBox *wordCOM;
    QComboBox *stopCOM;
    QComboBox *portCOM;
    QLabel *label_72;
    QLabel *label_73;
    QSpinBox *retriesCOM;
    QLabel *label_74;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_75;
    QSpinBox *responseDevice;
    QLabel *label_76;
    QSpinBox *addresDevice;
    QComboBox *parityCOM;
    QLabel *label_77;

    void setupUi(QDialog *SoftwareSetup)
    {
        if (SoftwareSetup->objectName().isEmpty())
            SoftwareSetup->setObjectName(QString::fromUtf8("SoftwareSetup"));
        SoftwareSetup->resize(368, 380);
        buttonBox = new QDialogButtonBox(SoftwareSetup);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(10, 340, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        tabWidget = new QTabWidget(SoftwareSetup);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 20, 351, 301));
        tabWidget->setAutoFillBackground(true);
        tabWidget->setStyleSheet(QString::fromUtf8("QTabBar::tab:hover {\n"
" \n"
"	background-color:#1C7ECC;\n"
"	border:none;\n"
"	color:white;\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
" \n"
"	background-color: rgb(22, 147, 204);\n"
"	border:none;\n"
"	color:white;\n"
"}\n"
"\n"
"\n"
"QTabBar::tab:!selected {\n"
"    margin-top: 2px; /* make non-selected tabs look smaller */\n"
"}\n"
"\n"
"QTabBar::tab {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"    border: 2px solid #C4C4C3;\n"
"    border-bottom-color: #C2C7CB; /* same as the pane color */\n"
"    border-top-left-radius: 4px;\n"
"    border-top-right-radius: 4px;\n"
"    min-width: 8ex;\n"
"    padding: 2px;\n"
"}"));
        interfaceSerial = new QWidget();
        interfaceSerial->setObjectName(QString::fromUtf8("interfaceSerial"));
        gridLayoutWidget_7 = new QWidget(interfaceSerial);
        gridLayoutWidget_7->setObjectName(QString::fromUtf8("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(10, 10, 311, 249));
        gridLayout_7 = new QGridLayout(gridLayoutWidget_7);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);
        label_69 = new QLabel(gridLayoutWidget_7);
        label_69->setObjectName(QString::fromUtf8("label_69"));

        gridLayout_7->addWidget(label_69, 0, 0, 1, 1);

        baudCOM = new QComboBox(gridLayoutWidget_7);
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->addItem(QString());
        baudCOM->setObjectName(QString::fromUtf8("baudCOM"));

        gridLayout_7->addWidget(baudCOM, 2, 1, 1, 1);

        label_70 = new QLabel(gridLayoutWidget_7);
        label_70->setObjectName(QString::fromUtf8("label_70"));

        gridLayout_7->addWidget(label_70, 6, 0, 1, 1);

        label_71 = new QLabel(gridLayoutWidget_7);
        label_71->setObjectName(QString::fromUtf8("label_71"));

        gridLayout_7->addWidget(label_71, 2, 0, 1, 1);

        wordCOM = new QComboBox(gridLayoutWidget_7);
        wordCOM->addItem(QString());
        wordCOM->addItem(QString());
        wordCOM->setObjectName(QString::fromUtf8("wordCOM"));

        gridLayout_7->addWidget(wordCOM, 3, 1, 1, 1);

        stopCOM = new QComboBox(gridLayoutWidget_7);
        stopCOM->addItem(QString());
        stopCOM->addItem(QString());
        stopCOM->setObjectName(QString::fromUtf8("stopCOM"));

        gridLayout_7->addWidget(stopCOM, 6, 1, 1, 1);

        portCOM = new QComboBox(gridLayoutWidget_7);
        portCOM->setObjectName(QString::fromUtf8("portCOM"));

        gridLayout_7->addWidget(portCOM, 0, 1, 1, 1);

        label_72 = new QLabel(gridLayoutWidget_7);
        label_72->setObjectName(QString::fromUtf8("label_72"));

        gridLayout_7->addWidget(label_72, 7, 0, 1, 1);

        label_73 = new QLabel(gridLayoutWidget_7);
        label_73->setObjectName(QString::fromUtf8("label_73"));

        gridLayout_7->addWidget(label_73, 8, 2, 1, 1);

        retriesCOM = new QSpinBox(gridLayoutWidget_7);
        retriesCOM->setObjectName(QString::fromUtf8("retriesCOM"));
        retriesCOM->setMaximum(10);

        gridLayout_7->addWidget(retriesCOM, 7, 1, 1, 1);

        label_74 = new QLabel(gridLayoutWidget_7);
        label_74->setObjectName(QString::fromUtf8("label_74"));

        gridLayout_7->addWidget(label_74, 3, 0, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer_7, 9, 0, 1, 1);

        label_75 = new QLabel(gridLayoutWidget_7);
        label_75->setObjectName(QString::fromUtf8("label_75"));

        gridLayout_7->addWidget(label_75, 8, 0, 1, 1);

        responseDevice = new QSpinBox(gridLayoutWidget_7);
        responseDevice->setObjectName(QString::fromUtf8("responseDevice"));
        responseDevice->setMaximum(5000);
        responseDevice->setSingleStep(100);

        gridLayout_7->addWidget(responseDevice, 8, 1, 1, 1);

        label_76 = new QLabel(gridLayoutWidget_7);
        label_76->setObjectName(QString::fromUtf8("label_76"));

        gridLayout_7->addWidget(label_76, 1, 0, 1, 1);

        addresDevice = new QSpinBox(gridLayoutWidget_7);
        addresDevice->setObjectName(QString::fromUtf8("addresDevice"));

        gridLayout_7->addWidget(addresDevice, 1, 1, 1, 1);

        parityCOM = new QComboBox(gridLayoutWidget_7);
        parityCOM->addItem(QString());
        parityCOM->addItem(QString());
        parityCOM->addItem(QString());
        parityCOM->setObjectName(QString::fromUtf8("parityCOM"));

        gridLayout_7->addWidget(parityCOM, 4, 1, 1, 1);

        label_77 = new QLabel(gridLayoutWidget_7);
        label_77->setObjectName(QString::fromUtf8("label_77"));

        gridLayout_7->addWidget(label_77, 4, 0, 1, 1);

        tabWidget->addTab(interfaceSerial, QString());

        retranslateUi(SoftwareSetup);
        QObject::connect(buttonBox, SIGNAL(accepted()), SoftwareSetup, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SoftwareSetup, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SoftwareSetup);
    } // setupUi

    void retranslateUi(QDialog *SoftwareSetup)
    {
        SoftwareSetup->setWindowTitle(QApplication::translate("SoftwareSetup", "Setup interfaces", nullptr));
        label_69->setText(QApplication::translate("SoftwareSetup", "Port interface:", nullptr));
        baudCOM->setItemText(0, QApplication::translate("SoftwareSetup", "1200", nullptr));
        baudCOM->setItemText(1, QApplication::translate("SoftwareSetup", "2400", nullptr));
        baudCOM->setItemText(2, QApplication::translate("SoftwareSetup", "4800", nullptr));
        baudCOM->setItemText(3, QApplication::translate("SoftwareSetup", "9600", nullptr));
        baudCOM->setItemText(4, QApplication::translate("SoftwareSetup", "19200", nullptr));
        baudCOM->setItemText(5, QApplication::translate("SoftwareSetup", "38400", nullptr));
        baudCOM->setItemText(6, QApplication::translate("SoftwareSetup", "57600", nullptr));
        baudCOM->setItemText(7, QApplication::translate("SoftwareSetup", "115200", nullptr));

        label_70->setText(QApplication::translate("SoftwareSetup", "Stop bits:", nullptr));
        label_71->setText(QApplication::translate("SoftwareSetup", "Baud rate:", nullptr));
        wordCOM->setItemText(0, QApplication::translate("SoftwareSetup", "7", nullptr));
        wordCOM->setItemText(1, QApplication::translate("SoftwareSetup", "8", nullptr));

        stopCOM->setItemText(0, QApplication::translate("SoftwareSetup", "1", nullptr));
        stopCOM->setItemText(1, QApplication::translate("SoftwareSetup", "2", nullptr));

        label_72->setText(QApplication::translate("SoftwareSetup", "Number of retries:", nullptr));
        label_73->setText(QApplication::translate("SoftwareSetup", "ms", nullptr));
        label_74->setText(QApplication::translate("SoftwareSetup", "Word lenght:", nullptr));
        label_75->setText(QApplication::translate("SoftwareSetup", "Response timeout:", nullptr));
        label_76->setText(QApplication::translate("SoftwareSetup", "Addres module:", nullptr));
        parityCOM->setItemText(0, QApplication::translate("SoftwareSetup", "NONE", nullptr));
        parityCOM->setItemText(1, QApplication::translate("SoftwareSetup", "EVEN", nullptr));
        parityCOM->setItemText(2, QApplication::translate("SoftwareSetup", "ODD", nullptr));

        label_77->setText(QApplication::translate("SoftwareSetup", "Parity:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(interfaceSerial), QApplication::translate("SoftwareSetup", "Serial interface", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SoftwareSetup: public Ui_SoftwareSetup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOFTWARESETUP_H

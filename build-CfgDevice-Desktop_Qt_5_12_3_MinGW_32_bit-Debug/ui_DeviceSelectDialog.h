/********************************************************************************
** Form generated from reading UI file 'DeviceSelectDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVICESELECTDIALOG_H
#define UI_DEVICESELECTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLayout>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DeviceSelectDialog
{
public:
    QLabel *label;
    QToolBox *AppDevices_toolBox;
    QWidget *pagePowerMeters;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QWidget *pageRecorders;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer;
    QWidget *pageTransducers;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QToolButton *MD04_button;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *DeviceSelectDialog)
    {
        if (DeviceSelectDialog->objectName().isEmpty())
            DeviceSelectDialog->setObjectName(QString::fromUtf8("DeviceSelectDialog"));
        DeviceSelectDialog->resize(558, 416);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DeviceSelectDialog->sizePolicy().hasHeightForWidth());
        DeviceSelectDialog->setSizePolicy(sizePolicy);
        DeviceSelectDialog->setStyleSheet(QString::fromUtf8("#DeviceSelectDialog{\n"
"	/*background-color: qlineargradient(spread:pad, x1:0.5, y1:0, x2:0.5, y2:1, stop:0 rgba(111, 113, 120, 255), stop:1 rgba(69, 70, 75, 255));*/\n"
"	background-color: white;\n"
"}\n"
"\n"
"QToolButton{	font: 75 14pt \"Arial\";}\n"
"QToolButton{background-color: rgba(255, 255, 255, 0);}\n"
"\n"
"\n"
"QToolBox::tab {\n"
"/*    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);*/\n"
"	background-color: rgb(160, 203, 231);\n"
"	font: bold 9pt \"Ariali\";\n"
"	color:gray;\n"
"}\n"
"\n"
"QToolBox::tab:selected { /* italicize selected tabs */\n"
"    color: black;\n"
"}\n"
""));
        label = new QLabel(DeviceSelectDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 557, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft YaHei"));
        font.setPointSize(10);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("font-weight: bold; background-color:#dcdcdc;\n"
"/*background-color: rgb(160, 203, 231)*/"));
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label->setMargin(3);
        AppDevices_toolBox = new QToolBox(DeviceSelectDialog);
        AppDevices_toolBox->setObjectName(QString::fromUtf8("AppDevices_toolBox"));
        AppDevices_toolBox->setEnabled(true);
        AppDevices_toolBox->setGeometry(QRect(10, 22, 531, 391));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Microsoft YaHei"));
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        AppDevices_toolBox->setFont(font1);
        AppDevices_toolBox->setCursor(QCursor(Qt::ArrowCursor));
        AppDevices_toolBox->setAutoFillBackground(false);
        AppDevices_toolBox->setStyleSheet(QString::fromUtf8("background-color: transparent\n"
"\n"
""));
        pagePowerMeters = new QWidget();
        pagePowerMeters->setObjectName(QString::fromUtf8("pagePowerMeters"));
        pagePowerMeters->setGeometry(QRect(0, 0, 531, 298));
        gridLayoutWidget = new QWidget(pagePowerMeters);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 553, 288));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 1, 0, 1, 1);

        AppDevices_toolBox->addItem(pagePowerMeters, QString::fromUtf8("Machines"));
        pageRecorders = new QWidget();
        pageRecorders->setObjectName(QString::fromUtf8("pageRecorders"));
        pageRecorders->setGeometry(QRect(0, 0, 531, 298));
        gridLayoutWidget_2 = new QWidget(pageRecorders);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(0, 0, 531, 162));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 0, 1, 1);

        AppDevices_toolBox->addItem(pageRecorders, QString::fromUtf8("Meters"));
        pageTransducers = new QWidget();
        pageTransducers->setObjectName(QString::fromUtf8("pageTransducers"));
        pageTransducers->setGeometry(QRect(0, 0, 531, 298));
        gridLayoutWidget_3 = new QWidget(pageTransducers);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(0, 0, 531, 288));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 0, 1, 1, 1);

        MD04_button = new QToolButton(gridLayoutWidget_3);
        MD04_button->setObjectName(QString::fromUtf8("MD04_button"));
        MD04_button->setEnabled(true);
        MD04_button->setMinimumSize(QSize(140, 140));
        MD04_button->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/_resources/devices/MD04.png"), QSize(), QIcon::Normal, QIcon::Off);
        MD04_button->setIcon(icon);
        MD04_button->setIconSize(QSize(100, 100));
        MD04_button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        gridLayout_3->addWidget(MD04_button, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 1, 0, 1, 1);

        AppDevices_toolBox->addItem(pageTransducers, QString::fromUtf8("Modules"));

        retranslateUi(DeviceSelectDialog);

        AppDevices_toolBox->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(DeviceSelectDialog);
    } // setupUi

    void retranslateUi(QDialog *DeviceSelectDialog)
    {
        DeviceSelectDialog->setWindowTitle(QApplication::translate("DeviceSelectDialog", "Select device type", nullptr));
        label->setText(QApplication::translate("DeviceSelectDialog", "Devices:", nullptr));
        AppDevices_toolBox->setItemText(AppDevices_toolBox->indexOf(pagePowerMeters), QApplication::translate("DeviceSelectDialog", "Machines", nullptr));
        AppDevices_toolBox->setItemText(AppDevices_toolBox->indexOf(pageRecorders), QApplication::translate("DeviceSelectDialog", "Meters", nullptr));
        MD04_button->setText(QApplication::translate("DeviceSelectDialog", "MD04", nullptr));
        AppDevices_toolBox->setItemText(AppDevices_toolBox->indexOf(pageTransducers), QApplication::translate("DeviceSelectDialog", "Modules", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DeviceSelectDialog: public Ui_DeviceSelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVICESELECTDIALOG_H

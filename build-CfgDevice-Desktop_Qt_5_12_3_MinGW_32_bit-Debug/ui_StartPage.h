/********************************************************************************
** Form generated from reading UI file 'StartPage.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTPAGE_H
#define UI_STARTPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_StartPage
{
public:
    QFrame *horizontalFrame;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_4;
    QLabel *PowerArchive;
    QLabel *versionLabel;

    void setupUi(QDialog *StartPage)
    {
        if (StartPage->objectName().isEmpty())
            StartPage->setObjectName(QString::fromUtf8("StartPage"));
        StartPage->resize(758, 446);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(StartPage->sizePolicy().hasHeightForWidth());
        StartPage->setSizePolicy(sizePolicy);
        StartPage->setStyleSheet(QString::fromUtf8("background-color:white;"));
        horizontalFrame = new QFrame(StartPage);
        horizontalFrame->setObjectName(QString::fromUtf8("horizontalFrame"));
        horizontalFrame->setGeometry(QRect(0, 0, 761, 451));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(horizontalFrame->sizePolicy().hasHeightForWidth());
        horizontalFrame->setSizePolicy(sizePolicy1);
        gridLayout_2 = new QGridLayout(horizontalFrame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 3, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 0, 2, 1, 1);

        label_4 = new QLabel(horizontalFrame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/_resources/images/background.png")));

        gridLayout_2->addWidget(label_4, 3, 2, 1, 1);

        PowerArchive = new QLabel(horizontalFrame);
        PowerArchive->setObjectName(QString::fromUtf8("PowerArchive"));
        PowerArchive->setMaximumSize(QSize(16777215, 50));
        PowerArchive->setStyleSheet(QString::fromUtf8("font: 32pt \"Simplified Arabic\";"));
        PowerArchive->setText(QString::fromUtf8("Configurator"));
        PowerArchive->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(PowerArchive, 1, 1, 1, 1);

        versionLabel = new QLabel(horizontalFrame);
        versionLabel->setObjectName(QString::fromUtf8("versionLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(versionLabel->sizePolicy().hasHeightForWidth());
        versionLabel->setSizePolicy(sizePolicy2);
        QFont font;
        font.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font.setPointSize(12);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        versionLabel->setFont(font);
        versionLabel->setStyleSheet(QString::fromUtf8("font: 12pt \"MS Shell Dlg 2\";\n"
"color: rgb(97, 97, 97);"));
        versionLabel->setText(QString::fromUtf8("v. 0.1.0"));
        versionLabel->setAlignment(Qt::AlignCenter);
        versionLabel->setMargin(20);

        gridLayout_2->addWidget(versionLabel, 2, 1, 1, 1);


        retranslateUi(StartPage);

        QMetaObject::connectSlotsByName(StartPage);
    } // setupUi

    void retranslateUi(QDialog *StartPage)
    {
        StartPage->setWindowTitle(QApplication::translate("StartPage", "Dialog", nullptr));
        label_4->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class StartPage: public Ui_StartPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTPAGE_H

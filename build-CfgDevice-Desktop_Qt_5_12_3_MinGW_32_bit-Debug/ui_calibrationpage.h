/********************************************************************************
** Form generated from reading UI file 'calibrationpage.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALIBRATIONPAGE_H
#define UI_CALIBRATIONPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_CalibrationPage
{
public:
    QTabWidget *tabCalib;
    QWidget *tab;
    QPushButton *btemp1_setP2_2;
    QLabel *label_18;
    QLineEdit *cp_p2_max;
    QPushButton *btemp1_setP1_2;
    QLabel *label_20;
    QLabel *label_21;
    QLineEdit *cp_p1_min;
    QLabel *label_22;
    QLineEdit *cp_p1_max;
    QLineEdit *cp_p2_min;
    QPushButton *bCalibCP;
    QPushButton *bCalibCP_Minus;
    QPushButton *btemp1_setPMin;
    QPushButton *btemp1_setPMax;
    QLabel *label_24;
    QLabel *label_19;
    QWidget *tabTEMP1;
    QLineEdit *temp1_pkt1_min;
    QLineEdit *temp1_pkt1_max;
    QPushButton *btemp1_setP1;
    QLabel *label_23;
    QPushButton *bCalibTemp1;
    QPushButton *btemp1_setPkt2;
    QLabel *label_33;
    QLabel *label_34;
    QWidget *tab_3;
    QLabel *label_29;
    QLabel *label_31;
    QLineEdit *temp2_pkt1_max;
    QLineEdit *temp2_pkt1_min;
    QLabel *label_32;
    QPushButton *btemp2_setP1;
    QPushButton *bCalibTemp2;
    QPushButton *bTemp2_setP2;
    QWidget *tab_2;
    QLabel *labDispT1;
    QLabel *labDispCP1;
    QLCDNumber *lcdTemp1;
    QLCDNumber *lcdCP;
    QPushButton *bMeausre;
    QLCDNumber *lcd_Temp2;
    QLabel *labDispT2;
    QCheckBox *checkZiarno;
    QCheckBox *checkZiarno_2;
    QLCDNumber *lcdCP_2;
    QLabel *labDispCP2;
    QCheckBox *checkZiarno_3;
    QPushButton *bMeausre_2;
    QWidget *tab_4;
    QCustomPlot *wykres;
    QComboBox *wyborWYkresu;
    QLabel *label;
    QPushButton *bCreatePlot;
    QLabel *labInfo;
    QPushButton *bCalib;
    QPushButton *bDisc;
    QLabel *label_2;
    QComboBox *wyborPortu;

    void setupUi(QWidget *CalibrationPage)
    {
        if (CalibrationPage->objectName().isEmpty())
            CalibrationPage->setObjectName(QString::fromUtf8("CalibrationPage"));
        CalibrationPage->resize(750, 483);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(230, 249, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(228, 228, 228, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(101, 101, 101, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(135, 135, 135, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush3);
        QBrush brush6(QColor(255, 255, 220, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush7(QColor(0, 0, 0, 128));
        brush7.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        QBrush brush8(QColor(202, 202, 202, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush7);
#endif
        CalibrationPage->setPalette(palette);
        CalibrationPage->setContextMenuPolicy(Qt::CustomContextMenu);
        CalibrationPage->setAutoFillBackground(false);
        CalibrationPage->setStyleSheet(QString::fromUtf8("QWidget{\n"
"	background-color: #e6f9ff;\n"
"	border-radius: 2px;\n"
"}\n"
"\n"
"\n"
"QPushButton{\n"
"	background-color: rgb(124, 157, 117);\n"
"	text-align: center;\n"
"	font: 10pt\"Microsoft YaHei UI\";\n"
"	font-weight: bold;\n"
"	color: white;\n"
"	border-radius: 4px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	background-color: #3399ff;\n"
"}\n"
"\n"
"QPushButton:disabled{\n"
"	 background-color: #bfbfbf;\n"
"}\n"
"\n"
"QComboBox{\n"
"	background-color: #d9d9d9;\n"
"	font: 10pt\"Microsoft YaHei UI\";\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"QLineEdit{\n"
"	text-align:center;\n"
"	font: 12pt\"Microsoft YaHei UI\";\n"
"	font-weight:bold;\n"
"	color:black;\n"
"	background-color:#d9d9d9;\n"
"	width: 100px;\n"
"	height: 30px;\n"
"}\n"
"\n"
"QLineEdit:hover{\n"
"	background-color: white;\n"
"}\n"
"QLabel{\n"
"	text-align: center;\n"
"	color: black;\n"
"}\n"
"\n"
"QLabel#label_cal1{\n"
"	font: 16 pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"#label_cal2{\n"
"	font: 16 pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"QTabWidget#t"
                        "ab{\n"
"	background-color: #0047b3;\n"
"}\n"
"\n"
"#labInfo{\n"
"	font-weight: bold;\n"
"	color: red;\n"
"}\n"
"\n"
"QLCDNumber{\n"
"	color: red;\n"
"	border-radius: 4px;\n"
"	background-color: black;\n"
"	border: 10px solid #bfbfbff;\n"
"}\n"
"\n"
"#labDisp1{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"\n"
"#labDisp2{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"\n"
"#label21{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"\n"
"#label22{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"#label23{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"\n"
"#label24{\n"
"	font: 14pt \"Microsoft YaHei UI\";\n"
"}\n"
"\n"
"QCheckBox{\n"
"	color: black;\n"
"}"));
        tabCalib = new QTabWidget(CalibrationPage);
        tabCalib->setObjectName(QString::fromUtf8("tabCalib"));
        tabCalib->setEnabled(true);
        tabCalib->setGeometry(QRect(40, 10, 641, 361));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        tabCalib->setPalette(palette1);
        tabCalib->setStyleSheet(QString::fromUtf8(""));
        tabCalib->setTabShape(QTabWidget::Rounded);
        tabCalib->setTabBarAutoHide(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        btemp1_setP2_2 = new QPushButton(tab);
        btemp1_setP2_2->setObjectName(QString::fromUtf8("btemp1_setP2_2"));
        btemp1_setP2_2->setEnabled(false);
        btemp1_setP2_2->setGeometry(QRect(380, 290, 101, 31));
        label_18 = new QLabel(tab);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(310, 80, 51, 21));
        QFont font;
        font.setPointSize(12);
        label_18->setFont(font);
        cp_p2_max = new QLineEdit(tab);
        cp_p2_max->setObjectName(QString::fromUtf8("cp_p2_max"));
        cp_p2_max->setEnabled(false);
        cp_p2_max->setGeometry(QRect(380, 240, 101, 21));
        btemp1_setP1_2 = new QPushButton(tab);
        btemp1_setP1_2->setObjectName(QString::fromUtf8("btemp1_setP1_2"));
        btemp1_setP1_2->setEnabled(false);
        btemp1_setP1_2->setGeometry(QRect(390, 130, 101, 31));
        label_20 = new QLabel(tab);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(100, 80, 61, 16));
        label_20->setFont(font);
        label_21 = new QLabel(tab);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(100, 180, 491, 41));
        QFont font1;
        font1.setPointSize(24);
        label_21->setFont(font1);
        cp_p1_min = new QLineEdit(tab);
        cp_p1_min->setObjectName(QString::fromUtf8("cp_p1_min"));
        cp_p1_min->setEnabled(false);
        cp_p1_min->setGeometry(QRect(170, 80, 101, 21));
        label_22 = new QLabel(tab);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(110, 10, 471, 41));
        label_22->setFont(font1);
        cp_p1_max = new QLineEdit(tab);
        cp_p1_max->setObjectName(QString::fromUtf8("cp_p1_max"));
        cp_p1_max->setEnabled(false);
        cp_p1_max->setGeometry(QRect(370, 80, 101, 21));
        cp_p2_min = new QLineEdit(tab);
        cp_p2_min->setObjectName(QString::fromUtf8("cp_p2_min"));
        cp_p2_min->setEnabled(false);
        cp_p2_min->setGeometry(QRect(170, 240, 111, 21));
        bCalibCP = new QPushButton(tab);
        bCalibCP->setObjectName(QString::fromUtf8("bCalibCP"));
        bCalibCP->setEnabled(false);
        bCalibCP->setGeometry(QRect(510, 230, 81, 41));
        bCalibCP_Minus = new QPushButton(tab);
        bCalibCP_Minus->setObjectName(QString::fromUtf8("bCalibCP_Minus"));
        bCalibCP_Minus->setEnabled(false);
        bCalibCP_Minus->setGeometry(QRect(510, 70, 81, 41));
        btemp1_setPMin = new QPushButton(tab);
        btemp1_setPMin->setObjectName(QString::fromUtf8("btemp1_setPMin"));
        btemp1_setPMin->setEnabled(false);
        btemp1_setPMin->setGeometry(QRect(170, 130, 101, 31));
        btemp1_setPMax = new QPushButton(tab);
        btemp1_setPMax->setObjectName(QString::fromUtf8("btemp1_setPMax"));
        btemp1_setPMax->setEnabled(false);
        btemp1_setPMax->setGeometry(QRect(170, 290, 101, 31));
        label_24 = new QLabel(tab);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(110, 240, 51, 21));
        label_24->setFont(font);
        label_19 = new QLabel(tab);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(320, 240, 51, 21));
        label_19->setFont(font);
        tabCalib->addTab(tab, QString());
        tabTEMP1 = new QWidget();
        tabTEMP1->setObjectName(QString::fromUtf8("tabTEMP1"));
        temp1_pkt1_min = new QLineEdit(tabTEMP1);
        temp1_pkt1_min->setObjectName(QString::fromUtf8("temp1_pkt1_min"));
        temp1_pkt1_min->setEnabled(false);
        temp1_pkt1_min->setGeometry(QRect(210, 120, 101, 21));
        temp1_pkt1_max = new QLineEdit(tabTEMP1);
        temp1_pkt1_max->setObjectName(QString::fromUtf8("temp1_pkt1_max"));
        temp1_pkt1_max->setEnabled(false);
        temp1_pkt1_max->setGeometry(QRect(430, 120, 101, 21));
        btemp1_setP1 = new QPushButton(tabTEMP1);
        btemp1_setP1->setObjectName(QString::fromUtf8("btemp1_setP1"));
        btemp1_setP1->setEnabled(false);
        btemp1_setP1->setGeometry(QRect(200, 200, 121, 41));
        label_23 = new QLabel(tabTEMP1);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(100, 40, 511, 61));
        label_23->setFont(font1);
        bCalibTemp1 = new QPushButton(tabTEMP1);
        bCalibTemp1->setObjectName(QString::fromUtf8("bCalibTemp1"));
        bCalibTemp1->setEnabled(false);
        bCalibTemp1->setGeometry(QRect(280, 270, 131, 41));
        btemp1_setPkt2 = new QPushButton(tabTEMP1);
        btemp1_setPkt2->setObjectName(QString::fromUtf8("btemp1_setPkt2"));
        btemp1_setPkt2->setEnabled(false);
        btemp1_setPkt2->setGeometry(QRect(410, 200, 121, 41));
        label_33 = new QLabel(tabTEMP1);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setGeometry(QRect(140, 120, 61, 16));
        label_33->setFont(font);
        label_34 = new QLabel(tabTEMP1);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setGeometry(QRect(350, 120, 71, 21));
        label_34->setFont(font);
        tabCalib->addTab(tabTEMP1, QString());
        temp1_pkt1_min->raise();
        temp1_pkt1_max->raise();
        label_23->raise();
        bCalibTemp1->raise();
        btemp1_setP1->raise();
        btemp1_setPkt2->raise();
        label_33->raise();
        label_34->raise();
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        label_29 = new QLabel(tab_3);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setGeometry(QRect(110, 40, 501, 61));
        label_29->setFont(font1);
        label_31 = new QLabel(tab_3);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setGeometry(QRect(130, 130, 61, 16));
        label_31->setFont(font);
        temp2_pkt1_max = new QLineEdit(tab_3);
        temp2_pkt1_max->setObjectName(QString::fromUtf8("temp2_pkt1_max"));
        temp2_pkt1_max->setEnabled(false);
        temp2_pkt1_max->setGeometry(QRect(410, 130, 101, 21));
        temp2_pkt1_min = new QLineEdit(tab_3);
        temp2_pkt1_min->setObjectName(QString::fromUtf8("temp2_pkt1_min"));
        temp2_pkt1_min->setEnabled(false);
        temp2_pkt1_min->setGeometry(QRect(200, 130, 101, 21));
        label_32 = new QLabel(tab_3);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setGeometry(QRect(340, 130, 71, 21));
        label_32->setFont(font);
        btemp2_setP1 = new QPushButton(tab_3);
        btemp2_setP1->setObjectName(QString::fromUtf8("btemp2_setP1"));
        btemp2_setP1->setEnabled(false);
        btemp2_setP1->setGeometry(QRect(200, 200, 121, 41));
        bCalibTemp2 = new QPushButton(tab_3);
        bCalibTemp2->setObjectName(QString::fromUtf8("bCalibTemp2"));
        bCalibTemp2->setEnabled(false);
        bCalibTemp2->setGeometry(QRect(280, 270, 131, 41));
        bTemp2_setP2 = new QPushButton(tab_3);
        bTemp2_setP2->setObjectName(QString::fromUtf8("bTemp2_setP2"));
        bTemp2_setP2->setEnabled(false);
        bTemp2_setP2->setGeometry(QRect(380, 200, 121, 41));
        tabCalib->addTab(tab_3, QString());
        label_29->raise();
        label_31->raise();
        temp2_pkt1_max->raise();
        temp2_pkt1_min->raise();
        label_32->raise();
        bCalibTemp2->raise();
        bTemp2_setP2->raise();
        btemp2_setP1->raise();
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        labDispT1 = new QLabel(tab_2);
        labDispT1->setObjectName(QString::fromUtf8("labDispT1"));
        labDispT1->setGeometry(QRect(30, 70, 191, 41));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font2.setPointSize(14);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setWeight(50);
        labDispT1->setFont(font2);
        labDispCP1 = new QLabel(tab_2);
        labDispCP1->setObjectName(QString::fromUtf8("labDispCP1"));
        labDispCP1->setGeometry(QRect(440, 30, 241, 41));
        labDispCP1->setFont(font2);
        lcdTemp1 = new QLCDNumber(tab_2);
        lcdTemp1->setObjectName(QString::fromUtf8("lcdTemp1"));
        lcdTemp1->setGeometry(QRect(20, 130, 171, 61));
        lcdCP = new QLCDNumber(tab_2);
        lcdCP->setObjectName(QString::fromUtf8("lcdCP"));
        lcdCP->setGeometry(QRect(460, 80, 161, 61));
        bMeausre = new QPushButton(tab_2);
        bMeausre->setObjectName(QString::fromUtf8("bMeausre"));
        bMeausre->setEnabled(false);
        bMeausre->setGeometry(QRect(100, 240, 141, 61));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        QBrush brush9(QColor(124, 157, 117, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush2);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush9);
        QBrush brush10(QColor(255, 255, 255, 128));
        brush10.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Active, QPalette::PlaceholderText, brush10);
#endif
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        QBrush brush11(QColor(255, 255, 255, 128));
        brush11.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush11);
#endif
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        QBrush brush12(QColor(191, 191, 191, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush12);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush12);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush12);
        QBrush brush13(QColor(255, 255, 255, 128));
        brush13.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush13);
#endif
        bMeausre->setPalette(palette2);
        QFont font3;
        font3.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setItalic(false);
        font3.setWeight(75);
        bMeausre->setFont(font3);
        bMeausre->setAutoDefault(false);
        bMeausre->setFlat(false);
        lcd_Temp2 = new QLCDNumber(tab_2);
        lcd_Temp2->setObjectName(QString::fromUtf8("lcd_Temp2"));
        lcd_Temp2->setGeometry(QRect(220, 130, 171, 61));
        labDispT2 = new QLabel(tab_2);
        labDispT2->setObjectName(QString::fromUtf8("labDispT2"));
        labDispT2->setGeometry(QRect(230, 70, 211, 41));
        labDispT2->setFont(font2);
        checkZiarno = new QCheckBox(tab_2);
        checkZiarno->setObjectName(QString::fromUtf8("checkZiarno"));
        checkZiarno->setEnabled(true);
        checkZiarno->setGeometry(QRect(90, 210, 81, 21));
        checkZiarno_2 = new QCheckBox(tab_2);
        checkZiarno_2->setObjectName(QString::fromUtf8("checkZiarno_2"));
        checkZiarno_2->setEnabled(true);
        checkZiarno_2->setGeometry(QRect(290, 210, 81, 21));
        lcdCP_2 = new QLCDNumber(tab_2);
        lcdCP_2->setObjectName(QString::fromUtf8("lcdCP_2"));
        lcdCP_2->setGeometry(QRect(460, 190, 161, 61));
        labDispCP2 = new QLabel(tab_2);
        labDispCP2->setObjectName(QString::fromUtf8("labDispCP2"));
        labDispCP2->setGeometry(QRect(440, 140, 231, 51));
        labDispCP2->setFont(font2);
        checkZiarno_3 = new QCheckBox(tab_2);
        checkZiarno_3->setObjectName(QString::fromUtf8("checkZiarno_3"));
        checkZiarno_3->setEnabled(true);
        checkZiarno_3->setGeometry(QRect(530, 260, 81, 21));
        bMeausre_2 = new QPushButton(tab_2);
        bMeausre_2->setObjectName(QString::fromUtf8("bMeausre_2"));
        bMeausre_2->setEnabled(false);
        bMeausre_2->setGeometry(QRect(270, 240, 141, 61));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush2);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush9);
        QBrush brush14(QColor(255, 255, 255, 128));
        brush14.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Active, QPalette::PlaceholderText, brush14);
#endif
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        QBrush brush15(QColor(255, 255, 255, 128));
        brush15.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush15);
#endif
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush12);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush12);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush12);
        QBrush brush16(QColor(255, 255, 255, 128));
        brush16.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        bMeausre_2->setPalette(palette3);
        bMeausre_2->setFont(font3);
        bMeausre_2->setAutoDefault(false);
        bMeausre_2->setFlat(false);
        tabCalib->addTab(tab_2, QString());
        bMeausre->raise();
        labDispT1->raise();
        labDispCP1->raise();
        lcdTemp1->raise();
        lcdCP->raise();
        lcd_Temp2->raise();
        labDispT2->raise();
        checkZiarno->raise();
        checkZiarno_2->raise();
        lcdCP_2->raise();
        labDispCP2->raise();
        checkZiarno_3->raise();
        bMeausre_2->raise();
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        wykres = new QCustomPlot(tab_4);
        wykres->setObjectName(QString::fromUtf8("wykres"));
        wykres->setGeometry(QRect(50, 20, 581, 271));
        wyborWYkresu = new QComboBox(tab_4);
        wyborWYkresu->setObjectName(QString::fromUtf8("wyborWYkresu"));
        wyborWYkresu->setGeometry(QRect(290, 300, 91, 21));
        label = new QLabel(tab_4);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(210, 310, 61, 16));
        bCreatePlot = new QPushButton(tab_4);
        bCreatePlot->setObjectName(QString::fromUtf8("bCreatePlot"));
        bCreatePlot->setEnabled(true);
        bCreatePlot->setGeometry(QRect(410, 300, 71, 31));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush2);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush9);
        QBrush brush17(QColor(255, 255, 255, 128));
        brush17.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Active, QPalette::PlaceholderText, brush17);
#endif
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        QBrush brush18(QColor(255, 255, 255, 128));
        brush18.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush18);
#endif
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush12);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush12);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush12);
        QBrush brush19(QColor(255, 255, 255, 128));
        brush19.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush19);
#endif
        bCreatePlot->setPalette(palette4);
        bCreatePlot->setFont(font3);
        bCreatePlot->setAutoDefault(false);
        bCreatePlot->setFlat(false);
        tabCalib->addTab(tab_4, QString());
        labInfo = new QLabel(CalibrationPage);
        labInfo->setObjectName(QString::fromUtf8("labInfo"));
        labInfo->setGeometry(QRect(510, 410, 181, 21));
        bCalib = new QPushButton(CalibrationPage);
        bCalib->setObjectName(QString::fromUtf8("bCalib"));
        bCalib->setEnabled(true);
        bCalib->setGeometry(QRect(280, 390, 91, 51));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush2);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush9);
        QBrush brush20(QColor(255, 255, 255, 128));
        brush20.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Active, QPalette::PlaceholderText, brush20);
#endif
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        QBrush brush21(QColor(255, 255, 255, 128));
        brush21.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush21);
#endif
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush12);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush12);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush12);
        QBrush brush22(QColor(255, 255, 255, 128));
        brush22.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush22);
#endif
        bCalib->setPalette(palette5);
        bCalib->setFont(font3);
        bCalib->setAutoDefault(false);
        bCalib->setFlat(false);
        bDisc = new QPushButton(CalibrationPage);
        bDisc->setObjectName(QString::fromUtf8("bDisc"));
        bDisc->setEnabled(true);
        bDisc->setGeometry(QRect(380, 390, 91, 51));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush9);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush2);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush2);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush9);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush9);
        QBrush brush23(QColor(255, 255, 255, 128));
        brush23.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Active, QPalette::PlaceholderText, brush23);
#endif
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush9);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush9);
        QBrush brush24(QColor(255, 255, 255, 128));
        brush24.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush24);
#endif
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush12);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush12);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush12);
        QBrush brush25(QColor(255, 255, 255, 128));
        brush25.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette6.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush25);
#endif
        bDisc->setPalette(palette6);
        bDisc->setFont(font3);
        bDisc->setAutoDefault(false);
        bDisc->setFlat(false);
        label_2 = new QLabel(CalibrationPage);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(140, 380, 31, 16));
        wyborPortu = new QComboBox(CalibrationPage);
        wyborPortu->setObjectName(QString::fromUtf8("wyborPortu"));
        wyborPortu->setGeometry(QRect(110, 410, 91, 21));

        retranslateUi(CalibrationPage);

        tabCalib->setCurrentIndex(1);
        bMeausre->setDefault(false);
        bMeausre_2->setDefault(false);
        bCreatePlot->setDefault(false);
        bCalib->setDefault(false);
        bDisc->setDefault(false);


        QMetaObject::connectSlotsByName(CalibrationPage);
    } // setupUi

    void retranslateUi(QWidget *CalibrationPage)
    {
        CalibrationPage->setWindowTitle(QApplication::translate("CalibrationPage", "Calibration Window", nullptr));
        btemp1_setP2_2->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 2", nullptr));
        label_18->setText(QApplication::translate("CalibrationPage", "U2 [V]:", nullptr));
        cp_p2_max->setText(QApplication::translate("CalibrationPage", "12", nullptr));
        btemp1_setP1_2->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 2  ", nullptr));
        label_20->setText(QApplication::translate("CalibrationPage", "U1 [V]:", nullptr));
        label_21->setText(QApplication::translate("CalibrationPage", "<html><head/><body><p>Punkt kalibracyjny napi\304\231cia CP+</p></body></html>", nullptr));
        cp_p1_min->setText(QApplication::translate("CalibrationPage", "-12", nullptr));
        label_22->setText(QApplication::translate("CalibrationPage", "Punkt kalibracyjny napi\304\231cia CP-", nullptr));
        cp_p1_max->setText(QApplication::translate("CalibrationPage", "0", nullptr));
        cp_p2_min->setText(QApplication::translate("CalibrationPage", "0", nullptr));
        bCalibCP->setText(QApplication::translate("CalibrationPage", "Kalibracja", nullptr));
        bCalibCP_Minus->setText(QApplication::translate("CalibrationPage", "Kalibracja", nullptr));
        btemp1_setPMin->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 1 ", nullptr));
        btemp1_setPMax->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 1 ", nullptr));
        label_24->setText(QApplication::translate("CalibrationPage", "U1 [V]:", nullptr));
        label_19->setText(QApplication::translate("CalibrationPage", "U2 [V]:", nullptr));
        tabCalib->setTabText(tabCalib->indexOf(tab), QApplication::translate("CalibrationPage", "Kalibracja napi\304\231cia", nullptr));
        temp1_pkt1_min->setText(QApplication::translate("CalibrationPage", "80", nullptr));
        temp1_pkt1_max->setText(QApplication::translate("CalibrationPage", "160", nullptr));
        btemp1_setP1->setText(QApplication::translate("CalibrationPage", " Ustaw pkt. 1 ", nullptr));
        label_23->setText(QApplication::translate("CalibrationPage", "Punkt kalibracyjny Temperatury 1:", nullptr));
        bCalibTemp1->setText(QApplication::translate("CalibrationPage", "Kalibracja temp. 1", nullptr));
        btemp1_setPkt2->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 2 ", nullptr));
        label_33->setText(QApplication::translate("CalibrationPage", "R1[\316\251]:", nullptr));
        label_34->setText(QApplication::translate("CalibrationPage", "R2 [\316\251]:", nullptr));
        tabCalib->setTabText(tabCalib->indexOf(tabTEMP1), QApplication::translate("CalibrationPage", "Kalibracja temperatury 1", nullptr));
        label_29->setText(QApplication::translate("CalibrationPage", "Punkt kalibracyjny Temperatury 2:", nullptr));
        label_31->setText(QApplication::translate("CalibrationPage", "R1[\316\251]:", nullptr));
        temp2_pkt1_max->setText(QApplication::translate("CalibrationPage", "1600", nullptr));
        temp2_pkt1_min->setText(QApplication::translate("CalibrationPage", "800", nullptr));
        label_32->setText(QApplication::translate("CalibrationPage", "R2 [\316\251]:", nullptr));
        btemp2_setP1->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 1", nullptr));
        bCalibTemp2->setText(QApplication::translate("CalibrationPage", "Kalibracja temp.2", nullptr));
        bTemp2_setP2->setText(QApplication::translate("CalibrationPage", "Ustaw pkt. 2 ", nullptr));
        tabCalib->setTabText(tabCalib->indexOf(tab_3), QApplication::translate("CalibrationPage", "Kalibracja Temperatury 2", nullptr));
        labDispT1->setText(QApplication::translate("CalibrationPage", "Rezystancja 1[\316\251]:", nullptr));
        labDispCP1->setText(QApplication::translate("CalibrationPage", "Warto\305\233\304\207 napi\304\231cia CP [V]:", nullptr));
        bMeausre->setText(QApplication::translate("CalibrationPage", "Wy\305\233wietl pomiary", nullptr));
        labDispT2->setText(QApplication::translate("CalibrationPage", "Rezystancja 2 [\316\251]:", nullptr));
        checkZiarno->setText(QApplication::translate("CalibrationPage", "Ziarno", nullptr));
        checkZiarno_2->setText(QApplication::translate("CalibrationPage", "Ziarno", nullptr));
        labDispCP2->setText(QApplication::translate("CalibrationPage", "Warto\305\233\304\207 napi\304\231cia CP [V]:", nullptr));
        checkZiarno_3->setText(QApplication::translate("CalibrationPage", "Ziarno", nullptr));
        bMeausre_2->setText(QApplication::translate("CalibrationPage", "Zatrzymaj pomiary", nullptr));
        tabCalib->setTabText(tabCalib->indexOf(tab_2), QApplication::translate("CalibrationPage", "Pomiary", nullptr));
        label->setText(QApplication::translate("CalibrationPage", "Wielko\305\233\304\207:", nullptr));
        bCreatePlot->setText(QApplication::translate("CalibrationPage", "Rysuj", nullptr));
        tabCalib->setTabText(tabCalib->indexOf(tab_4), QApplication::translate("CalibrationPage", "Wykresy", nullptr));
        labInfo->setText(QString());
        bCalib->setText(QApplication::translate("CalibrationPage", "Po\305\202\304\205cz", nullptr));
        bDisc->setText(QApplication::translate("CalibrationPage", "Roz\305\202\304\205cz", nullptr));
        label_2->setText(QApplication::translate("CalibrationPage", "Port:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CalibrationPage: public Ui_CalibrationPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALIBRATIONPAGE_H

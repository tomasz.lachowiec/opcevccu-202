/********************************************************************************
** Form generated from reading UI file 'ConsoleWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONSOLEWIDGET_H
#define UI_CONSOLEWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConsoleWidget
{
public:
    QGridLayout *gridLayout_2;
    QFrame *titleFrame;
    QHBoxLayout *horizontalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QToolButton *clearButton;
    QToolButton *closeButton;
    QTextEdit *textEdit;

    void setupUi(QWidget *ConsoleWidget)
    {
        if (ConsoleWidget->objectName().isEmpty())
            ConsoleWidget->setObjectName(QString::fromUtf8("ConsoleWidget"));
        ConsoleWidget->resize(578, 200);
        ConsoleWidget->setMaximumSize(QSize(16777215, 250));
        ConsoleWidget->setWindowTitle(QString::fromUtf8(""));
        ConsoleWidget->setLayoutDirection(Qt::LeftToRight);
        ConsoleWidget->setStyleSheet(QString::fromUtf8("#titleFrame{border:none;}"));
        gridLayout_2 = new QGridLayout(ConsoleWidget);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(1, 0, 1, 0);
        titleFrame = new QFrame(ConsoleWidget);
        titleFrame->setObjectName(QString::fromUtf8("titleFrame"));
        titleFrame->setMinimumSize(QSize(0, 24));
        titleFrame->setMaximumSize(QSize(16777215, 24));
        titleFrame->setStyleSheet(QString::fromUtf8("/*background-color: rgb(217, 255, 255);*/\n"
"background-color: #dcdcdc;\n"
""));
        titleFrame->setFrameShape(QFrame::StyledPanel);
        titleFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(titleFrame);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(2, -1, -1, -1);
        label = new QLabel(titleFrame);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font.setPointSize(10);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("font-weight: bold; background-color:#dcdcdc;"));
        label->setMargin(0);

        horizontalLayout->addWidget(label);

        clearButton = new QToolButton(titleFrame);
        clearButton->setObjectName(QString::fromUtf8("clearButton"));
        clearButton->setEnabled(true);
        clearButton->setCursor(QCursor(Qt::PointingHandCursor));
        clearButton->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        clearButton->setText(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/_resources/icons/blocked.png"), QSize(), QIcon::Normal, QIcon::Off);
        clearButton->setIcon(icon);
        clearButton->setIconSize(QSize(12, 12));

        horizontalLayout->addWidget(clearButton);

        closeButton = new QToolButton(titleFrame);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setEnabled(false);
        closeButton->setCursor(QCursor(Qt::PointingHandCursor));
        closeButton->setStyleSheet(QString::fromUtf8("background-color: rgba(255, 255, 255, 0);"));
        closeButton->setText(QString::fromUtf8(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/_resources/icons/close.png"), QSize(), QIcon::Normal, QIcon::Off);
        closeButton->setIcon(icon1);
        closeButton->setIconSize(QSize(10, 10));

        horizontalLayout->addWidget(closeButton);


        horizontalLayout_2->addLayout(horizontalLayout);


        gridLayout_2->addWidget(titleFrame, 0, 0, 1, 1);

        textEdit = new QTextEdit(ConsoleWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        textEdit->setFrameShape(QFrame::NoFrame);
        textEdit->setFrameShadow(QFrame::Sunken);
        textEdit->setReadOnly(true);
        textEdit->setHtml(QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Courier New'; font-size:10pt;\"><br /></p></body></html>"));

        gridLayout_2->addWidget(textEdit, 1, 0, 1, 1);


        retranslateUi(ConsoleWidget);

        QMetaObject::connectSlotsByName(ConsoleWidget);
    } // setupUi

    void retranslateUi(QWidget *ConsoleWidget)
    {
        label->setText(QApplication::translate("ConsoleWidget", "Console", nullptr));
#ifndef QT_NO_TOOLTIP
        clearButton->setToolTip(QApplication::translate("ConsoleWidget", "Clear", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        closeButton->setToolTip(QApplication::translate("ConsoleWidget", "Close", nullptr));
#endif // QT_NO_TOOLTIP
        Q_UNUSED(ConsoleWidget);
    } // retranslateUi

};

namespace Ui {
    class ConsoleWidget: public Ui_ConsoleWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONSOLEWIDGET_H
